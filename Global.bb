Global windowMode, videoColorDepth, curWindowMode
Global curIdiom, gameSound, gameMusic
Global modsAmount, curModId
Global g_width = 800
Global g_height = 600
Global g_rate# = float(g_height)/g_width
Global g_checkWidth = g_width - 10
Global g_checkheight = g_height - 10
Global g_Debug = 0;flag of debug
Global showBlowArea=0;
Global m_curSelected = 1;current selected board
Global m_curSelectedSwitch = 0;current selected board
Global fntPlayerName=LoadFont("Cambria",16,True,False,False)
Global g_Frames = 0
global maxAmap						= 100
global lastAmap						= 50;beat this map to beat the game
global maxVsMap						= 16
global maxCTFMap					= 16

global window
global canvas
Global message, messageN, mapN
Global totalSecrets, noAirStrike
Global pawnAmount,choosemap,gameLives, map, map_,backg,title,curMap,sndStr$,loadOnce,Tn,strWarning$,Warning,WarnSeq, mapRestart
Global buttonAmount,gmStr$,gameCommand,mapAmount,lastgamemode,butNA,butHum,butCPU, mapComplete, secretsFound,secretsAmount
Global fontType=1, fontSpace=1, previousMap, screenShotN
Global quake, quakeSeq,mapsLoaded,xScr,yScr,xScr_,yScr_,scrollMap,scrollXspeed#, scrollYspeed#, scrLock
Global fightMode, xScrStart, yScrStart, noAirSpecial, noDoubleJump, fileBkp, noItems
Global Famount
Global curAni,tAniAmount,taniMEnu
Global areaMoves, saAreaMovesAmount,daAreaMovesAmount
Global rectAmount
Global colorR,colorG,colorB,bg,bgAmount=5
Global triggerAmount,triggerMode, triggerImageAmount,amountAffected
Global zamountPlaying,flagAmount,itenAmount, shotamount, objAmount,chunkAmount,platAmount,boxAmount,expAmount
Global wallAmount,gameDone, aliveAmountNeeded, prevZAmountPlaying,prevZzamount
Global twoPlayersKeyb,bgColor,maxScore,ScoreDone,showVidMem,level,renderdelay
Global xTileImg#,yTileImg#,winner,flagMaxTime,showZColor,teamAttack,vsGameMode, vsMode 
Global gamePaused,b_joyhit,timePassed#, keypressed, keyschosen,pn,ifiniteLives,flagMaxScore,targetMaxScore
Global endGame,gameTime,gameTime2,NoUserInput,tarN,areaAmount,dAreaAmount,objFrequency, alwaysSpawnObj
Global rScrLimit=1400,lScrLimit=-760,uScrLimit=-50000,dScrLimit=540, yScrCameraBottomLimit
Global rendert, renderFreq, maxObjAmount
Global menuOption, duringGameMenu

Global img_but_ta_bkgnd=LoadImage(gfxStuff$ + "but_ta.bmp")
Global img_but_gm_bkgnd=LoadImage(gfxStuff$ + "but_gm.bmp")
Global img_but_start_bkgnd=LoadImage(gfxStuff$ + "but_start.bmp")
Global img_but_sound_bkgnd=LoadImage(gfxStuff$ + "but_sound.bmp")
Global img_but_team_bkgnd=LoadImage(gfxStuff$ + "but_team.bmp")
Global arrow1=LoadImage(gfxStuff$ + "arrow1.bmp")
Global arrow2=LoadImage(gfxStuff$ + "arrow2.bmp")
Global greenSign=LoadImage(gfxStuff$ + "sign.bmp")
Global pad=LoadImage(gfxStuff$ + "pad.bmp")
Global board=LoadImage(gfxStuff$ + "board.bmp")
Global board2=LoadImage(gfxStuff$ + "board2.bmp")
Global board3=LoadImage(gfxStuff$ + "board3.bmp")
Global board_selector = LoadAnimImage(gfxStuff$ + "select_board.bmp",132,163,0,2)
Global lock=LoadImage(gfxStuff$ + "lock.bmp")
Global noPic=LoadImage(gfxStuff$ + "no.bmp")
Global pic_team_red = LoadImage(gfxStuff$ + "team_red.bmp")
Global pic_team_green = LoadImage(gfxStuff$ + "team_green.bmp")
Global frameTimer = CreateTimer(52) ;define FPS game will run
Global obj1P=LoadImage(gfxStuff$ + "obj1_1.bmp")
Global obj2P=LoadImage(gfxStuff$ + "obj2_1.bmp")
Global objEnergy = LoadImage(gfxStuff$ + "obj_Energy.bmp")
Global obj3P=LoadImage(gfxStuff$ + "obj3_1.bmp")
Global obj4p=LoadImage(gfxStuff$ + "obj4_1.bmp")
Global obj5p=LoadImage(gfxStuff$ + "obj5_1.bmp")
Global obj8p=LoadImage(gfxStuff$ + "obj8_1.bmp")
Global obj9p=LoadImage(gfxStuff$ + "obj9_1.bmp")
Global obj9p_=LoadImage(gfxStuff$ + "obj9_1_.bmp")
Global objArrow=LoadImage(gfxStuff$ + "objArrow.bmp")
Global flag1P=LoadImage(gfxStuff$ + "flag1.bmp")
Global flag2P=LoadImage(gfxStuff$ + "flag2.bmp")
Global controllerPic=LoadImage(gfxStuff$ + "controller.bmp")
Global keyboardPic=LoadImage(gfxStuff$ + "keyboard.bmp")
Global intro=LoadSound(soundsdir$ + "intro.mp3")
Global victorySnd=LoadSound(soundsdir$ + "victory.mp3")
Global sDoorSnd=LoadSound(soundsdir$ + "sDoor.mp3")
Global mDoorSnd=LoadSound(soundsdir$ + "mDoor.mp3")
Global energySnd=LoadSound(soundsdir$ + "energy.mp3")
Global batsSnd=LoadSound(soundsdir$ + "bats.mp3")
Global dragonRoarSnd
Global bazookaSnd=LoadSound(soundsdir$ + "bazooka.mp3")
Global shockSnd=LoadSound(soundsdir$ + "shock.mp3")
Global RaySnd=LoadSound(soundsdir$ + "ray.mp3")
Global flyBySnd=LoadSound(soundsdir$ + "flyBy.mp3")
Global jokerSnd
Global predHitSnd=LoadSound(soundsdir$ + "predHit.mp3")
Global PredatorRaySnd=LoadSound(soundsdir$ + "predatorRay.mp3")
Global PredatorSnd
Global NoSnd=LoadSound(soundsdir$ + "NoSound.mp3")
Global ReadySnd=LoadSound(soundsdir$ + "ready.mp3")
Global FightSnd=LoadSound(soundsdir$ + "fight.mp3")
Global clapSnd=LoadSound(soundsdir$ + "clap.mp3")
Global ohclapSnd=LoadSound(soundsdir$ + "ohclap.mp3")
Global eeeeSnd=LoadSound(soundsdir$ + "eeee.mp3")
Global TucupSnd=LoadSound(soundsdir$ + "tucup.mp3")
Global ThrowSnd=LoadSound(soundsdir$ + "Throw.mp3")
Global gotShotSnd=LoadSound(soundsdir$ + "gotShot.mp3")
Global vooSnd=LoadSound(soundsdir$ + "voo.mp3")
Global smashSnd=LoadSound(soundsdir$ + "smash.mp3")
Global blow2Snd=LoadSound(soundsdir$ + "blow2.mp3")
Global bhitSnd=LoadSound(soundsdir$ + "bhit.mp3")
Global CapeSnd
Global RashHitSnd=LoadSound(soundsdir$ + "RashHit.mp3")
Global MarioUahaSnd
Global MarioWeakSnd=LoadSound(soundsdir$ + "marioWeak.mp3")
Global MarioFierceSnd=LoadSound(soundsdir$ + "marioFierce.mp3")
Global ddHitSnd=LoadSound(soundsdir$ + "ddHit.mp3")
Global shredderSnd
Global shredder2Snd
Global mikePunchSnd=LoadSound(soundsdir$ + "mikePunch.mp3")
Global mikeKickSnd=LoadSound(soundsdir$ + "mikeKick.mp3")
Global mikeSnd
Global mikeUpperCutSnd
Global mikeFlipSnd
Global mikeBreathSnd
Global spiderstingsnd
Global huasnd
Global swordSnd
Global hayabusaSnd
Global shurikenSnd=LoadSound(soundsdir$ + "shuriken.mp3")
Global webshotsnd
Global webhitsnd=LoadSound(soundsdir$ + "webhit.mp3")
Global fireballsnd=LoadSound(soundsdir$ + "fireball.mp3")
Global firehitsnd=LoadSound(soundsdir$ + "firehit.mp3")
Global hiasnd
Global hiahuusnd
Global coinsnd=LoadSound(soundsdir$ + "coin.mp3")
Global hueSnd
Global uppercutsnd
Global mariouppercutsnd
Global brokensnd=LoadSound(soundsdir$ + "broken.mp3")
Global blockedsnd=LoadSound(soundsdir$ + "blocked.mp3")
Global kicksnd=LoadSound(soundsdir$ + "highkick.mp3")
Global blowsnd=LoadSound(soundsdir$ + "highBlow.mp3")
Global blowsnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\punch.mp3")
Global kicksnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\kick_effect.mp3")
Global uppercutsnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\voice3.mp3")
Global knifesnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\knife_effect3.mp3")
Global knifesnd2_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\knife_effect.mp3")
Global knifesnd3_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\knife_effect2.mp3")
Global knocksnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\huya.mp3")
Global supersnd_zhaoyun=LoadSound(soundsdir$ + "\\character\\zhaoyun\\yina.mp3")
Global blowvoice_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\yiya.mp3")

Global blowsnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\punch.mp3")
Global kick_sound_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\high_kick.mp3")
Global flykicksnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\ya.mp3")
Global lowkicksnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\leg.mp3")
Global normalkicksnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\ha.mp3")
Global youjinken_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\youjinken.mp3")
Global uppercut1_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\uppercut1.mp3")
Global uppercut2_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\uppercut2.mp3")
Global leg_rocket_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\leg_rocket.mp3")
Global dragon_ball_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\dragon_ball.mp3")
Global hyuken_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\hyuken.mp3")
Global suoyouha_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\suoyouha.mp3")
Global dragon_fly_snd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\dragon_flying.mp3")
Global knocksnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\huya.mp3")
Global supersnd_jimmy=LoadSound(soundsdir$ + "\\character\\jimmy\\yina.mp3")

Global attacksnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\attack.mp3")
Global blocksnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\block.mp3")
Global fly_attacksnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\fly_attack.mp3")
Global low_attacksnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\low_attack.mp3")
Global rushsnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\rush.mp3")
Global specialsnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\special.mp3")
Global supersnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\super.mp3")
Global upblowsnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\upblow.mp3")
Global uppercutsnd_guanyu=LoadSound(soundsdir$ + "\\character\\guanyu\\uppercut.mp3")

Global highpunchsnd=LoadSound(soundsdir$ + "highpunch.mp3")
Global puch_effect_zhaoyun=LoadSound(soundsdir$ + "punch_effect.mp3")
Global lasersnd=LoadSound(soundsdir$ + "laser.mp3")
Global shotsnd=LoadSound(soundsdir$ + "shot.mp3")
Global shotwallsnd=LoadSound(soundsdir$ + "shotwall.mp3")
Global zhitwallsnd=LoadSound(soundsdir$ + "zhitwall.mp3")
Global ryuBallsnd
Global ryuSpinsnd
Global whipSnd, crossSnd, richterSnd, fastThrowSnd
Global Snd, goku1Snd, teleportSnd
Global dbzHitSnd=LoadSound(soundsdir$ + "dbzHit.mp3")
Global explodesnd=LoadSound(soundsdir$ + "explode.mp3")
Global clicksnd=LoadSound(soundsdir$ + "click.mp3")
Global slashsnd=LoadSound(soundsdir$ + "slash.mp3")
Global ctfSnd=LoadSound(soundsdir$ + "ctf.mp3")
Global pickupSnd=LoadSound(soundsdir$ + "pickup.mp3")
Global menuMusic
Global justIntroduced
Global music
Global music2
Global chMusic
Global chMusic2
Global musicN1
Global musicN2
Global timestart#=MilliSecs()
Global gframe,fps
Global milli#