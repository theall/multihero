;main configure
Const g_appTitle$					= "MultiHero 0.96 beta for theall"
Const gameVersion$					= "0.96 SHARED"
Const characterAmount				= 24;Add character, 1=ryu, 2=rash ... change the value from 10 to 11, 11=your new character id
Const g_playerCounts				= 10;player select counts
const available_hero_counts			= 12;
Const Nfps							= 52;52 Number of frames per second the game should render


;resource path
Const gfxRoot$						= "gfx\"
Const gfxStuff$						= "gfx\stuff\"
Const gfxTile$						= "gfx\tiles\"
Const mapsBaseDir$					= "maps\"
Const soundsdir$					= "sounds\"

;helper type
const count_helper					= 4
const helper_none					= 0
const helper_laser_turret			= 1
const helper_bombing_ship			= 2
const helper_ray_balls_canon		= 3
const helper_flying_bats			= 4

;index of sprite image
const index_sprite_walk				= 1
const index_sprite_falling			= 2
const index_sprite_duck				= 3
const index_sprite_air				= 4
const index_sprite_Flip				= 5
const index_sprite_blow				= 6
const index_sprite_up_special		= 7
const index_sprite_fly_kick			= 8
const index_sprite_low_kick			= 9
const index_sprite_special			= 10
const index_sprite_shot				= 11
const index_sprite_down_special 	= 12
const index_sprite_block			= 13
const index_sprite_upblow			= 14
const index_sprite_grab				= 15
const index_sprite_super			= 16
const index_sprite_no_action		= 17
const index_sprite_super_pic		= 20

;const weapon
const weapon_ryu_ball								= 5
const weapon_web_shot								= 6
const weapon_fire_ball								= 7
const weapon_laser									= 8
const weapon_mike_dragon_fire_ball					= 9
const weapon_bullet									= 10
const weapon_rash_axe								= 11
const weapon_rash_axe_0								= 12
const weapon_fire_ball_0							= 13
const weapon_fire_ball_special						= 14
const weapon_ryu_hayabusa_ninja_star				= 15
const weapon_ninja_star_uturn_boomerang_effect		= 16
const weapon_red_horns_uturn_fire					= 17
const weapon_pink_blob								= 18
const weapon_bowser_fire_ball						= 19
const weapon_batrang								= 20
const weapon_predator_green_ray						= 21
const weapon_predator_disc							= 22
const weapon_missile_0								= 23
const weapon_big_laser								= 24
const weapon_ray_blue_ball							= 25
const weapon_evil_black_missile						= 26
const weapon_cannon_ball							= 27
const weapon_goku_ball								= 28
const weapon_ritcher_cross							= 29
const weapon_ritcher_sword							= 30
const weapon_knife									= 31
const weapon_jimmy_dragon_ball						= 32
const weapon_jimmy_dragon_double					= 33

;const of object type
Const object_max_amounts							= 21;Amount of existing objects
const object_yellow_shell							= 1
const object_medkit									= 2
const object_green_shell							= 3
const object_explosive_barrel						= 4  
const object_helper									= 5  
const object_club									= 6  
const object_gun									= 7  
const object_acid_spit								= 8
const object_batman_little_bomb						= 9
const object_bazooka								= 10
const object_ray_gun								= 11  
const object_ray_ball								= 12
const object_hammer									= 13
const object_dragon_fire_ball						= 14
const object_cannon_ball							= 15
const object_flying_bat								= 16
const object_big_rock								= 17
const object_little_rock							= 18
const object_little_lava_rock						= 19
const object_axe									= 20
const object_type_energy_pill						= 21

;chunk type
const chunk_type_amounts							= 100
const chunk_frame_max_amounts						= 20
const chunk_type_dragon_ball						= 61
const chunk_type_jimmy_attack						= 62
const chunk_type_zhaoyun_knife						= 63
const chunk_type_dragon_double						= 64

;action
const action_blocking				= 0
const action_punch					= 1
const action_flying_kick			= 2
const action_low_kick				= 4
const action_uppercut				= 5
const action_throwing_item			= 6
const action_special				= 7
const action_dogding				= 8
const action_down_special			= 9
const action_high_kick				= 10
const action_club					= 11
const action_shooting_position		= 12
const action_item_pickup			= 13
const action_super_special			= 14
const action_throw					= 15   

;characters
const character_ryu					= 1
const character_rash				= 2
const character_spiderman			= 3
const character_mario				= 4
const character_mike				= 5
const character_gaiden				= 6
const character_batman				= 7
const character_predator			= 8
const character_goku				= 9
const character_ritcher				= 10
const character_jimmy				= 11
const character_zhaoyun				= 12
const character_guanyu				= 13
const character_usa_captain			= 24
const character_pig					= 30
const character_alien				= 31
const character_footclan			= 32
const character_shredder			= 33
const character_thug				= 34
const character_redhorns			= 35
const character_gargola				= 36
const character_redplant			= 37
const character_bowser				= 38
const character_thief				= 39
const character_turtle				= 40
const character_turtlecloud			= 41
const character_joker				= 42
const character_laserhelper			= 43
const character_venom				= 44
const character_bombingship			= 45
const character_rayballs			= 46
const character_soldier				= 47
const character_cylinder			= 48
const character_dragon				= 49
const character_laserbeam			= 50
const character_bag					= 51

const menu_interval = 40

;the index of button rect
;scene:main
const index_main_adventure_mode 		= 1
const index_main_vs_mode 				= 2
const index_main_options 				= 3
const index_main_credits				= 4
const index_main_exit					= 5

;scene:player select
const index_player_select_team_attack		= 1
const index_player_select_game_mode			= 2
const index_player_select_lives_left		= 3
const index_player_select_lives_right		= 4
const index_player_select_items_switch		= 5
const index_player_select_start_game		= 6

const index_player_select_character1		= 11;from 11 to 10+characterAmount

const index_player_select_player_type1		= 35
const index_player_select_player_team1		= 45
const index_player_select_player_arrow1_l	= 55
const index_player_select_player_arrow1_r	= 65
const index_player_select_level1			= 75
const index_player_select_selector1			= 85

;scene options
const index_options_full_screen				= 1
const index_options_video					= 2
const index_options_sfx						= 3
const index_options_music					= 4
const index_options_controls				= 5
const index_options_language				= 6
const index_options_back					= 7
const index_options_maximaze				= 7

;scene stage select
const index_stage_select_arrow_l			= 1
const index_stage_select_arrow_r			= 2
const index_stage_select_map1				= 3

;scene:in game menu
const index_in_game_resume					= 1
const index_in_game_options					= 2
const index_in_game_main_menu				= 3
const index_in_game_exit_game				= 4

;scene:control config
const index_control_joy_type1				= 1;from 1 to 4
const index_control_config1					= 5;from 5 to 8

;menu text index
const index_str_start_game						= 1
const index_str_team_attack_on					= 2
const index_str_team_attack_off					= 3
const index_str_sound_on						= 4
const index_str_sound_off						= 5
const index_str_death_match						= 6
const index_str_capture_the_flag				= 7
const index_str_keep_the_flag					= 8
const index_str_hit_the_target					= 9
const index_str_time							= 10
const index_str_score							= 11
const index_str_player							= 12
const index_str_won								= 13
const index_str_adventure_mode					= 14
const index_str_options							= 15
const index_str_language						= 16
const index_str_game_mode						= 17
const index_str_video							= 18
const index_str_full_screen						= 19
const index_str_window_mode						= 20
const index_str_vs_mode							= 21
const index_str_stage_select					= 22
const index_str_loading							= 23
const index_str_red_team_won					= 24
const index_str_green_team_won					= 25
const index_str_red								= 26
const index_str_green							= 27
const index_str_team_won						= 28
const index_str_lives							= 29
const index_str_team_none						= 30
const index_str_team							= 31
const index_str_level							= 32
const index_str_all_players_must_have_a_team	= 33
const index_str_draw_game						= 34
const index_str_secret_areas					= 35
const index_str_stage_complete					= 36
const index_str_press_any_key					= 37
const index_str_hits_taken						= 38
const index_str_untouchable						= 39
const index_str_credits							= 40
const index_str_on								= 41
const index_str_off								= 42
const index_str_16_bits							= 43
const index_str_32_bits							= 44
const index_str_sfx								= 45
const index_str_music							= 46
const index_str_english							= 47
const index_str_back							= 48
const index_str_controls						= 49
const index_str_exit							= 50
const index_str_up								= 51
const index_str_down							= 52
const index_str_left							= 53
const index_str_right							= 54
const index_str_attack							= 55
const index_str_special							= 56
const index_str_jump							= 57
const index_str_block							= 58
const index_str_throw							= 59
const index_str_config							= 60
const index_str_new_character_unlocked			= 61
const index_str_new_vs_stage_unlocked			= 62
const index_str_resume							= 63
const index_str_main_menu						= 64
const index_str_exit_game						= 65
const index_str_must_restart_to_take_effect		= 66
const index_str_press_jump_while_in				= 67
const index_str_the_air_to_double_jump			= 68
const index_str_press_up_special				= 69
const index_str_while_in_the_air_to				= 70
const index_str_go_up_even_further				= 71
const index_str_press_attack_or_special			= 72
const index_str_to_fight_you_can_use			= 73
const index_str_combinations_by_pressing		= 74
const index_str_up_or_down_ex_down_special		= 75
const index_str_stand_close_to_the_switch		= 76
const index_str_and_press_up_to_use_it			= 77
const index_str_stand_on_an_item_and			= 78
const index_str_press_attack_to_pick_it_up		= 79
const index_str_then_press_throw_or_attack		= 80
const index_str_to_throw_it						= 81
const index_str_to_go_down_from_platforms		= 82
const index_str_press_down_jump_when			= 83
const index_str_standing_on_it					= 84
const index_str_you_can_throw_items_at			= 85
const index_str_different_directions_even		= 86
const index_str_diagonally_simply_hold			= 87
const index_str_the_direction_you_wish_to		= 88
const index_str_throw_and_press_attack			= 89
const index_str_when_the_red_bar_at_the_top		= 90
const index_str_of_the_screen_turns_green		= 91
const index_str_you_can_release_a_super_move	= 92
const index_str_by_pressing_block_special		= 93
const index_str_no_air_special					= 94
const index_str_secrets_found					= 95
const index_str_items_on						= 96
const index_str_items_off						= 97
const index_str_continue						= 98
const index_str_at_least_two_teams				= 99
const index_str_players_needed					= 100

;const of mode options
const menu_none						= 0
const menu_select_character			= 1
const menu_main						= 2
const menu_options					= 3
const menu_control					= 4
const menu_in_game					= 5

;const of game mode
const mode_vs_death_match			= 1
const mode_vs_catch_flag			= 2
const mode_vs_keep_flag				= 3
const mode_vs_hit_target			= 4

;const language index
const lang_english	= 1
const lang_chinese	= 2

;const command of game
const cmd_none						= 0
const cmd_game_start				= 1
const cmd_menu_start				= 2
const cmd_change_module				= 3

;const method of close screen
const method_none					= 0
const method_horizontal_line		= 1
const method_vertical_line			= 2
const method_horizontal_vertical	= 3
const method_vertical_spread		= 4

;others
const player_type_none						= 0
const player_type_cpu						= 1
const player_type_human						= 2

;team
const team_none						= 0
const team_red						= 1
const team_green					= 2