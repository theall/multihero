

;-------get directories For all mods -------
;load mods
Function setModDirs()

	; Define what folder to start with ...
	folder$ = mapsBaseDir$

	; Open up the directory, and assign the handle to myDir
	myDir=ReadDir(folder$)
	; Let's loop forever Until we run out of files/folders to list!
	n = 1
	modFolder$(1) = folder$+"Original"+"\"
	modsAmount=1
	Repeat
	; Assign the Next entry in the folder to file$
	file$=NextFile$(myDir)

	file$=Lower(file$)
	If file$="." Or file$=".." Or file$="original" Then
		
	Else
		If file$="" Then Exit
		; Use FileType to determine If it is a folder (value 2)
		If FileType(folder$+file$) = 2 Then
			n=n+1
			modFolder$(n) = folder$+file$+"\"
			modName(n) = Lower(file$)
			modsAmount=modsAmount+1
			;print n+"="+modFolder$(n)
		End If
	EndIf

	Forever
	; Properly close the open folder
	CloseDir myDir

End Function 
;-------save maps/secret stuff ----
Function saveMaps()
file = WriteFile(modFolder(curModId) + "save.dat")

For i=1 To maxAmap
    WriteInt file, mapOpen(i)
    WriteInt file, mapSecret(i)
Next
For i=1 To maxVsmap
    WriteInt file, vsMapOpen(i)
Next
For i=1 To maxCTFmap
    WriteInt file, CTFmapOpen(i)
Next

CloseFile file

End Function 
;--------- load maps/secret stuff -------
Function loadMaps()

If FileType(modFolder(curModId) + "save.dat")=0 Then
	temp=WriteFile(modFolder(curModId) + "save.dat")	;create file
	For n=1 To 500
		WriteInt temp, 0
	Next
	CloseFile temp
EndIf

file=ReadFile(modFolder(curModId) + "save.dat")

For i=1 To maxAmap
    mapOpen(i) = ReadInt (file)
    mapSecret(i) = ReadInt (file)
    totalSecrets = totalSecrets + mapSecret(i)
Next
For i=1 To maxVsmap
    vsMapOpen(i) = ReadInt (file)
Next
For i=1 To maxCTFmap
    CTFmapOpen(i) = ReadInt (file)
Next

For n=1 To characterAmount	;unlock first 20 characters by default for any mod
	if n=<available_hero_counts Then characterOpen(n)=1
Next

If curModId = 1 Then
	mapOpen(1)=1	;unlock first adventure stage by default
	For n=1 To 4	;unlock first 4 versus levels
		vsmapOpen(n)=1
	Next
	CTFmapOpen(1)=1	;unlock first CTF stage
Else
	mapOpen(1)=1	;unlock first adventure stage by default
	For i=1 To 50	;unlock all versus levels
		vsmapOpen(i)=1
		CTFmapOpen(i)=1
	Next
EndIf

CloseFile file

End Function 

;------------------save general maps data----
Function saveData()
file = WriteFile("maps.dat")

WriteInt file, maxAmap
WriteInt file, maxVsMap
WriteInt file, maxCTFMap
WriteInt file, lastAmap
WriteInt file, 0
WriteInt file, 0
WriteInt file, 0
WriteInt file, 0
WriteInt file, 0
WriteInt file, 0

CloseFile file

End Function 
;-------Load general maps data ---------------------
Function loadData()

file=ReadFile("maps.dat")

maxAmap = ReadInt (file)
maxVsMap = ReadInt (file)
maxCTFMap = ReadInt (file)
lastAmap = ReadInt (file)

CloseFile file

End Function 

;------------------- Start menu For vs stage Select ---------------------------------
Function vsSelectMap()

	ClsColor 0,0,100
	Color 200,200,200   ;For the white rectangles around thumbnails
	buttonAmount=54 ;maxAmap
	SetBuffer CanvasBuffer(canvas)

	Repeat
		checkCLose()

		If KeyHit(1) Then;escape
			gameCommand = cmd_menu_start
			If vsmode=0 Then
			    defineButtons(1)
			Else
			    defineButtons(0)
			EndIf
		EndIf
		pointers
		
		n = 0
		
		If clickedBut(index_stage_select_arrow_l) Or clickedBut(index_stage_select_arrow_r) Then	;clicked arrow to change mod
			If gamesound Then PlaySound clicksnd
			If clickedBut(index_stage_select_arrow_r) Then
				curModId=curModId+1
				If curModId > modsAmount Then curModId=1
			Else
				curModId=curModId-1
				If curModId < 1 Then curModId=modsAmount
			EndIf
			loadMaps()

			gameCommand = cmd_change_module
		EndIf
		
		For i=index_stage_select_map1 To index_stage_select_map1+ButtonAmount-1
			n = n + 1
			If vsMode=0 Then open(n)=mapOpen(n)
			If vsMode=1 Then
				If vsGameMode=mode_vs_catch_flag Then
					open(n)=CTFmapOpen(n)
				Else
				    open(n)=VsmapOpen(n)
				EndIf
			EndIf

			If clickedBut(i) And open(n)=1 Then
				If gamesound Then PlaySound clicksnd
				curMap=n:gameCommand=cmd_game_start
			EndIf
		Next

		;-------stage menu rendering ----------
		Cls
		x = 15
		y = x * g_rate#
		drawTextImage x,y,img_but_start_bkgnd,strinfo$(index_str_stage_select)

		fontType=1
		x = priW(modName(curModId))
		w = strWidth(modName(curModId))
		h = strHeight(modName(curModId))
		y = y + (ImageHeight(img_but_start_bkgnd)-h)/2
		pri x, y, modName(curModId)
		fontType=2
		
		;draws arrows for selecting mod
		y = y + (h-ImageHeight(arrow1))/2
		old_x = x
		x = x - ImageWidth(arrow1) - 5
		drawImage_n arrow1,x,y,index_stage_select_arrow_l
		x = old_x + w + 5
		drawImage_n arrow2,x,y,index_stage_select_arrow_r

		;draw maps list
		ccount_line = 5
		if vsMode=0 then ccount_line=10;adventure mode ,10 maps per line
		spX=20 : spY=spX*g_rate    ;x/y space between maps
		If vsMode=0 Then
			spX=10 : spY=5
		EndIf

		mapW = ImageWidth(mapTn(1))
		mapH = ImageHeight(mapTn(1))
		offsetX = (g_width - ccount_line*(mapW+spX) + spX)/2
		offsetY = offsetX * g_rate#
		
		For n = 1 To maxAmap ;maxAmap
			index = index_stage_select_map1 + n -1
			x = (n-1) mod ccount_line * (mapW + spX) + offsetX
			y = (n-1) / ccount_line * (mapH + spY) + offsetY
			If vsMode=0 Then  ;If adventrure mode
				If mapOpen(n)=1 And mapTn(n) <> 0 Then
					DrawImage_n mapTn(n),x,y,index
				EndIf
			;If mapOpen(n)=1 And mapTn(n) <> 0 Then Rect x,y,wBut(n),hBut(n),0
		  	ElseIf vsGameMode = mode_vs_catch_flag Then  ;If CTF mode
				If CTFmapOpen(n)=1 And mapTn(n) <> 0 Then
					DrawImage_n mapTn(n),x,y,index
				EndIf
				Color 200,200,200
				If CTFmapOpen(n)=1 And mapTn(n) <> 0 Then Rect x,y,ImageWidth(mapTn(n)),ImageHeight(mapTn(n)) ,0

			ElseIf vsGameMode <> mode_vs_catch_flag Then ;If any other vs mode
				If vsmapOpen(n)=1 And mapTn(n) <> 0 Then
					DrawImage_n mapTn(n),x,y,index
				EndIf
				Color 200,200,200
				If vsmapOpen(n)=1 And mapTn(n) <> 0 Then Rect x,y,ImageWidth(mapTn(n)),ImageHeight(mapTn(n)) ,0

			EndIf
		Next

		For n=1 To 4
			DrawImage pointerPic(n),xpointer(n),ypointer(n)
		Next

		FlipCanvas canvas
	Until gameCommand <> cmd_none

End Function

;----- draws box ---------------
Function textBox(w,h)
	n1=g_width-(w) : x=(n1/2)
	n2=g_height-(h) : y=(n2/2)

	Color 0,0,0
	Rect x,y,w,h,1
	Color 255,255,255
	Rect x,y,w,h,0
	Color 120,120,120
	Rect x+1,y+1,w-2,h-2,0
	Return y
End Function

;------------ displays after stage screen ----------------
Function statsScreen()

	SetBuffer CanvasBuffer(canvas)
	statsImg=LoadImage(gfxStuff$ + "stats.bmp")
	imgW# = ImageWidth(statsImg)
	imgH# = ImageHeight(statsImg)
	If imgW#<>g_width or imgH#<>g_height
		ScaleImage statsImg,g_width/imgW#,g_height/imgH#
	EndIf
	
	ClsColor 0,50,100
	Cls
	Color 255,255,255
	DrawImage statsImg,0,0
	;n1=g_width-(priW(strInfo$(36))) : x=(n1/2)
	x = 0
	y = 80
	pri priW(strInfo$(index_str_stage_complete)),y, strInfo$(index_str_stage_complete)

	If checkWhatsOpen()=1 Then pri 100,100, strInfo$(61)

	x = 160
	y = x * g_rate
	pri x,y, strInfo$(index_str_secret_areas)+" "+secretsFound+" / "+secretsAmount

	y = y + strHeight(strInfo$(index_str_secret_areas)) + 20
	For i=1 To g_playerCounts
		If zwason(i)=1 Then
			drawimage zIcon(curGuy(i)),x-ImageWidth(zIcon(curGuy(i)))-10,y
			s$ = strInfo$(index_str_hits_taken) + zGotHitsAmount(i)
			pri x,y, s$
			If zGotHitsAmount(i)=0 Then pri x+strWidth(s$)+10,y,strInfo$(index_str_untouchable)
			y = y + strHeight(strInfo$(index_str_hits_taken)) + 15
		EndIf 
	Next

	y = g_height - strHeight(strInfo$(index_str_press_any_key)) - 80
	pri priW(strInfo$(index_str_press_any_key)),y, strInfo$(index_str_press_any_key)

	FlipCanvas canvas
	Delay 100
	FlushKeys : flushjoy
	waitInput()
	closeScreen(Rand(1,4),0)

	FreeImage statsImg

End Function

;------------ versus results ----------------
Function vsStatsScreen()

	SetBuffer CanvasBuffer(canvas)
	statsImg=LoadImage(gfxStuff$ + "stats.bmp")
	imgW# = ImageWidth(statsImg)
	imgH# = ImageHeight(statsImg)
	If imgW#<>g_width or imgH#<>g_height
		ScaleImage statsImg,g_width/imgW#,g_height/imgH#
	EndIf
	
	ClsColor 0,50,100
	Cls
	Color 255,255,255
	DrawImage statsImg,0,0
	
	y = 100
	If winner=1000 Then ;If its a draw
		pri priW(strInfo(index_str_draw_game)),y, strInfo$(index_str_draw_game)
	Else
		Select vsGameMode
			Case mode_vs_catch_flag;check CTF winner
				If winner=1 Then ts$= Strinfo$(index_str_red) Else ts$= Strinfo$(index_str_green)
				pri priW(strInfo(index_str_team_won)+ts$),y, ts$ + Strinfo$(index_str_team_won)
			Default ;check winner
				If zTeam(winner)=team_red Then
					pri priW(strInfo(index_str_red_team_won)),y, Strinfo$(index_str_red_team_won)
				EndIf
				If zTeam(winner)=team_green Then
					pri priW(strInfo(index_str_green_team_won)),y, Strinfo$(index_str_green_team_won)
				EndIf
				If zteam(winner) <> team_red And zteam(winner) <> team_green Then
					drawimage zPic(curGuy(winner),1,0),(priW(strInfo(index_str_player)+winner+Strinfo$(index_str_won)))-45 ,135-imageheight(zPic(curGuy(winner),1,0))
					pri priW(strInfo(index_str_player)+winner+Strinfo$(index_str_won)),y, Strinfo$(index_str_player) +winner+ Strinfo$(index_str_won)
				EndIf
		End Select
	EndIf

	x = 180
	y = y + strHeight("") + 40
	For i=1 To g_playerCounts
		If zwason(i)=1 Then
			;Print "i="+i+" curGuy(i)="+curGuy(i)
			;Print "zIcon(curGuy(i))="+zIcon(curGuy(i))
			If zIcon(curGuy(i))<>0 
				DrawImage zIcon(curGuy(i)),x-ImageWidth(zIcon(curGuy(i)))-10,y
				s$ = strInfo$(index_str_hits_taken) + zGotHitsAmount(i)
				pri x,y, s$
				If zGotHitsAmount(i)=0 Then pri x+strWidth(s$)+10,y,strInfo$(index_str_untouchable)
				y = y + strHeight(strInfo$(index_str_hits_taken)) + 15
			EndIf
		EndIf
	Next

	y = g_height - strHeight(strInfo$(index_str_press_any_key)) - 80
	pri priW(strInfo$(index_str_continue)),y, strInfo$(index_str_continue)

	FlipCanvas canvas
	Delay 100
	FlushKeys() : flushjoy()
	waitInput()
	closeScreen(Rand(1,4),0)
	FreeImage statsImg
End Function

;--------- wait any key/joy button to be pressed ----------
Function waitInput()
	pressed=0
	flushkeys() : flushjoy()
	Repeat
	 If KeyHit(1) Then pressed=1
	 If KeyHit(28) Then pressed=1
	 If KeyHit(57) Then pressed=1
	 For n=1 To 4
	  Select zController(n)
	   Case 0
		If KeyHit(shotK(n)) Or KeyHit(specialK(n)) Then pressed=1
	   Case 1
		If JoyHit(shotK(n),controllerPort(n)) Or JoyHit(specialK(n),controllerPort(n)) Then pressed=1
	  End Select
	 Next
	Delay(10)
	Until pressed=1
	flushkeys() : flushjoy()

End Function 

Function pri_n(x,y,st$,index)
	;recording the rect of drawing text
	If fillPosition(x, y, strWidth(st$), strHeight(st$), index)=1
		Return pri(x,y,st$)
	EndIf
	Return 0
End Function

;------------- draws font------------------------------
Function pri(x,y,st$)
	xstart=x
	For i=1 To Len(st$)
	  Select Mid$(st$,i,1)
		Case "0" : DrawImage letter(fontType,0),x,y : x = x + (letterWidth(fontType,0) + fontSpace)
		Case "1" : DrawImage letter(fontType,1),x,y : x = x + (letterWidth(fontType,1) + fontSpace)
		Case "2" : DrawImage letter(fontType,2),x,y : x = x + (letterWidth(fontType,2) + fontSpace)
		Case "3" : DrawImage letter(fontType,3),x,y : x = x + (letterWidth(fontType,3) + fontSpace)
		Case "4" : DrawImage letter(fontType,4),x,y : x = x + (letterWidth(fontType,4) + fontSpace)
		Case "5" : DrawImage letter(fontType,5),x,y : x = x + (letterWidth(fontType,5) + fontSpace)
		Case "6" : DrawImage letter(fontType,6),x,y : x = x + (letterWidth(fontType,6) + fontSpace)
		Case "7" : DrawImage letter(fontType,7),x,y : x = x + (letterWidth(fontType,7) + fontSpace)
		Case "8" : DrawImage letter(fontType,8),x,y : x = x + (letterWidth(fontType,8) + fontSpace)
		Case "9" : DrawImage letter(fontType,9),x,y : x = x + (letterWidth(fontType,9) + fontSpace)
		Case "a" : DrawImage letter(fontType,10),x,y : x = x + (letterWidth(fontType,10) + fontSpace)
		Case "b" : DrawImage letter(fontType,11),x,y : x = x + (letterWidth(fontType,11) + fontSpace)
		Case "c" : DrawImage letter(fontType,12),x,y : x = x + (letterWidth(fontType,12) + fontSpace)
		Case "d" : DrawImage letter(fontType,13),x,y : x = x + (letterWidth(fontType,13) + fontSpace)
		Case "e" : DrawImage letter(fontType,14),x,y : x = x + (letterWidth(fontType,14) + fontSpace)
		Case "f" : DrawImage letter(fontType,15),x,y : x = x + (letterWidth(fontType,15) + fontSpace)
		Case "g" : DrawImage letter(fontType,16),x,y : x = x + (letterWidth(fontType,16) + fontSpace)
		Case "h" : DrawImage letter(fontType,17),x,y : x = x + (letterWidth(fontType,17) + fontSpace)
		Case "i" : DrawImage letter(fontType,18),x,y : x = x + (letterWidth(fontType,18) + fontSpace)
		Case "j" : DrawImage letter(fontType,19),x,y : x = x + (letterWidth(fontType,19) + fontSpace)
		Case "k" : DrawImage letter(fontType,20),x,y : x = x + (letterWidth(fontType,20) + fontSpace)
		Case "l" : DrawImage letter(fontType,21),x,y : x = x + (letterWidth(fontType,21) + fontSpace)
		Case "m" : DrawImage letter(fontType,22),x,y : x = x + (letterWidth(fontType,22) + fontSpace)
		Case "n" : DrawImage letter(fontType,23),x,y : x = x + (letterWidth(fontType,23) + fontSpace)
		Case "o" : DrawImage letter(fontType,24),x,y : x = x + (letterWidth(fontType,24) + fontSpace)
		Case "p" : DrawImage letter(fontType,25),x,y : x = x + (letterWidth(fontType,25) + fontSpace)
		Case "q" : DrawImage letter(fontType,26),x,y : x = x + (letterWidth(fontType,26) + fontSpace)
		Case "r" : DrawImage letter(fontType,27),x,y : x = x + (letterWidth(fontType,27) + fontSpace)
		Case "s" : DrawImage letter(fontType,28),x,y : x = x + (letterWidth(fontType,28) + fontSpace)
		Case "t" : DrawImage letter(fontType,29),x,y : x = x + (letterWidth(fontType,29) + fontSpace)
		Case "u" : DrawImage letter(fontType,30),x,y : x = x + (letterWidth(fontType,30) + fontSpace)
		Case "v" : DrawImage letter(fontType,31),x,y : x = x + (letterWidth(fontType,31) + fontSpace)
		Case "x" : DrawImage letter(fontType,32),x,y : x = x + (letterWidth(fontType,32) + fontSpace)
		Case "w" : DrawImage letter(fontType,33),x,y : x = x + (letterWidth(fontType,33) + fontSpace)
		Case "y" : DrawImage letter(fontType,34),x,y : x = x + (letterWidth(fontType,34) + fontSpace)
		Case "z" : DrawImage letter(fontType,35),x,y : x = x + (letterWidth(fontType,35) + fontSpace)
		Case "." : DrawImage letter(fontType,36),x,y : x = x + (letterWidth(fontType,36) + fontSpace)
		Case "," : DrawImage letter(fontType,37),x,y : x = x + (letterWidth(fontType,37) + fontSpace)
		Case "-" : DrawImage letter(fontType,38),x,y : x = x + (letterWidth(fontType,38) + fontSpace)
		Case "(" : DrawImage letter(fontType,39),x,y : x = x + (letterWidth(fontType,39) + fontSpace)
		Case ")" : DrawImage letter(fontType,40),x,y : x = x + (letterWidth(fontType,40) + fontSpace)
		Case "/" : DrawImage letter(fontType,41),x,y : x = x + (letterWidth(fontType,41) + fontSpace)
		Case " " : DrawImage letter(fontType,42),x,y : x = x + (letterWidth(fontType,42) + fontSpace)
		Case "+" : DrawImage letter(fontType,43),x,y : x = x + (letterWidth(fontType,43) + fontSpace)
		Case "*" : DrawImage letter(fontType,44),x,y : x = x + (letterWidth(fontType,44) + fontSpace)
		Case "&" : DrawImage letter(fontType,45),x,y : x = x + (letterWidth(fontType,45) + fontSpace)
		Case "=" : DrawImage letter(fontType,46),x,y : x = x + (letterWidth(fontType,46) + fontSpace)
		Case ":" : DrawImage letter(fontType,47),x,y : x = x + (letterWidth(fontType,47) + fontSpace)
		Case "!" : DrawImage letter(fontType,48),x,y : x = x + (letterWidth(fontType,48) + fontSpace)
		Case "?" : DrawImage letter(fontType,49),x,y : x = x + (letterWidth(fontType,49) + fontSpace)
		Case "'" : DrawImage letter(fontType,50),x,y : x = x + (letterWidth(fontType,50) + fontSpace)
	    Case "?" : DrawImage letter(fontType,51),x,y : x = x + (letterWidth(fontType,51) + fontSpace)
		Case "?" : DrawImage letter(fontType,52),x,y : x = x + (letterWidth(fontType,52) + fontSpace)
		Case "?" : DrawImage letter(fontType,53),x,y : x = x + (letterWidth(fontType,53) + fontSpace)

		Default
		x=x+12

		End Select
	Next
	
	Return x - xstart

End Function 

Function strWidth(s$)
	w=0

	For i=1 To Len(s$)
		Select Mid(s$,i,1)
			Case "0": w=w+letterWidth(fontType,0)+ fontSpace
			Case "1": w=w+letterWidth(fontType,1)+ fontSpace
			Case "2": w=w+letterWidth(fontType,2)+ fontSpace
			Case "3": w=w+letterWidth(fontType,3)+ fontSpace
			Case "4": w=w+letterWidth(fontType,4)+ fontSpace
			Case "5": w=w+letterWidth(fontType,5)+ fontSpace
			Case "6": w=w+letterWidth(fontType,6)+ fontSpace
			Case "7": w=w+letterWidth(fontType,7)+ fontSpace
			Case "8": w=w+letterWidth(fontType,8)+ fontSpace
			Case "9": w=w+letterWidth(fontType,9)+ fontSpace
			Case "a": w=w+letterWidth(fontType,10)+ fontSpace
			Case "b": w=w+letterWidth(fontType,11)+ fontSpace
			Case "c": w=w+letterWidth(fontType,12)+ fontSpace
			Case "d": w=w+letterWidth(fontType,13)+ fontSpace
			Case "e": w=w+letterWidth(fontType,14)+ fontSpace
			Case "f": w=w+letterWidth(fontType,15)+ fontSpace
			Case "g": w=w+letterWidth(fontType,16)+ fontSpace
			Case "h": w=w+letterWidth(fontType,17)+ fontSpace
			Case "i": w=w+letterWidth(fontType,18)+ fontSpace
			Case "j": w=w+letterWidth(fontType,19)+ fontSpace
			Case "k": w=w+letterWidth(fontType,20)+ fontSpace
			Case "l": w=w+letterWidth(fontType,21)+ fontSpace
			Case "m": w=w+letterWidth(fontType,22)+ fontSpace
			Case "n": w=w+letterWidth(fontType,23)+ fontSpace
			Case "o": w=w+letterWidth(fontType,24)+ fontSpace
			Case "p": w=w+letterWidth(fontType,25)+ fontSpace
			Case "q": w=w+letterWidth(fontType,26)+ fontSpace
			Case "r": w=w+letterWidth(fontType,27)+ fontSpace
			Case "s": w=w+letterWidth(fontType,28)+ fontSpace
			Case "t": w=w+letterWidth(fontType,29)+ fontSpace
			Case "u": w=w+letterWidth(fontType,30)+ fontSpace
			Case "v": w=w+letterWidth(fontType,31)+ fontSpace
			Case "x": w=w+letterWidth(fontType,32)+ fontSpace
			Case "w": w=w+letterWidth(fontType,33)+ fontSpace
			Case "y": w=w+letterWidth(fontType,34)+ fontSpace
			Case "z": w=w+letterWidth(fontType,35)+ fontSpace
			Case ".": w=w+letterWidth(fontType,36)+ fontSpace
			Case ",": w=w+letterWidth(fontType,37)+ fontSpace
			Case "-": w=w+letterWidth(fontType,38)+ fontSpace
			Case "(": w=w+letterWidth(fontType,39)+ fontSpace
			Case ")": w=w+letterWidth(fontType,40)+ fontSpace
			Case "/": w=w+letterWidth(fontType,41)+ fontSpace
			Case " ": w=w+letterWidth(fontType,42)+ fontSpace
			Case "+": w=w+letterWidth(fontType,43)+ fontSpace
			Case "*": w=w+letterWidth(fontType,44)+ fontSpace
			Case "&": w=w+letterWidth(fontType,45)+ fontSpace
			Case "=": w=w+letterWidth(fontType,46)+ fontSpace
			Case ":": w=w+letterWidth(fontType,47)+ fontSpace
			Case "!": w=w+letterWidth(fontType,48)+ fontSpace
			Case "?": w=w+letterWidth(fontType,49)+ fontSpace
			Case "'": w=w+letterWidth(fontType,50)+ fontSpace
			Case "?": w=w+letterWidth(fontType,51)+ fontSpace
			Case "?": w=w+letterWidth(fontType,52)+ fontSpace
			Case "?": w=w+letterWidth(fontType,53)+ fontSpace
			Default
				w=w+letterWidth(fontType,0)*2+ fontSpace
		End Select
	Next

	Return w
End Function

Function strHeight(s$)
	h=letterHeight(fontType,0)
	Return h
End Function

;----------- get word width ------------------------------------------
Function priW(s$)
	w = strWidth(s$)
	n1=g_width-w :w = (n1/2)
	return w
End Function 

;----------- get word width ------------------------------------------
Function priH(s$)

End Function 

;----------------- Select player/ check If locked ---------------------------
Function selectPlayer(i,n,dir)
	;If i=1 Then print "zon:"+zOn(1)
	If zOn(i)<>0
		;get max character opened index
		l = 1
		For l=1 to characterAmount
			If characterOpen(l)=0
				l = l-1
				GoTo getMaxCharacterOpenedIndex
			EndIf
		Next
		
		.getMaxCharacterOpenedIndex
		If dir=1 Then di=1 Else di=-1   ;Select Next or previous
			oldGuy=curGuy(i)
			curGuy(i)=curGuy(i)+di
			If curGuy(i) < 1 Then curGuy(i) = l
			If curGuy(i) > l Then curGuy(i) = 1
			;print "current:"+curGuy(i)
			If characterOpen(curGuy(i))=0 Then curGuy(i) = oldGuy
			zThumbNail(i)=butpic(n)
	EndIf
End Function

;----------- MENU (character select)----------------------------------------
Function menuCharacterSelect()
	pointers

	bPlaySound = false
	For n=1 To ButtonAmount
		If clickedBut(n) Then
			;print xBut(n)+"."+yBut(n)+"."+wBut(n)+"."+hBut(n)
			Select n;Add character, add CASE 11 for your new guy, make you set curGuy(clickedBy(n) to 11 in the new line
				Case index_player_select_team_attack
					If teamAttack=1 Then
						teamAttack=0
					Else
						teamAttack=1
					EndIf
				Case index_player_select_game_mode;Select game mode on vs
					vsGameMode = vsGameMode + 1
					If vsGameMode > mode_vs_hit_target Then vsGameMode = mode_vs_death_match
					If vsGameMode=mode_vs_death_match Then flagAmount=0:gmStr$=strInfo$(index_str_death_match)
					If vsGameMode=mode_vs_catch_flag Then flagAmount=2:gmStr$=strInfo$(index_str_capture_the_flag)
					If vsGameMode=mode_vs_keep_flag Then flagAmount=1:gmStr$=strInfo$(index_str_keep_the_flag)
					If vsGameMode=mode_vs_hit_target Then flagAmount=0:gmStr$=strInfo$(index_str_hit_the_target)
				Case index_player_select_lives_left;Select how many points / lives / time
					Select vsGameMode
						Case mode_vs_death_match
							gameLives=gameLives-1
							If gameLives < 1 Then gameLives=99
						Case mode_vs_catch_flag
							flagMaxScore=flagMaxScore-1
							If flagMaxScore < 1 Then flagMaxScore=99
						Case mode_vs_keep_flag
							flagMaxTime=flagMaxTime-50
							If flagMaxTime < 1 Then flagMaxTime=1000
						Case mode_vs_hit_target
							targetMaxScore=targetMaxScore-1
							If targetMAxScore < 1 Then targetMAxScore=99
					End Select
				Case index_player_select_lives_right;Select how many points / lives / time
					Select vsGameMode
						Case mode_vs_death_match
							gameLives=gameLives+1
							If gameLives >99 Then gameLives=1
						Case mode_vs_catch_flag
							flagMaxScore=flagMaxScore+1
							If flagMaxScore >99 Then flagMaxScore=1
						Case mode_vs_keep_flag
							flagMaxTime=flagMaxTime+50
							If flagMaxTime > 1000 Then flagMaxTime=50
						Case mode_vs_hit_target
							targetMaxScore=targetMaxScore+1
							If targetMAxScore > 99 Then targetMAxScore=1
					End Select
				Case index_player_select_items_switch;TOGGLE ITEMS ON / OFF
					If noItems=1 Then
						noItems=0
					Else
						noItems=1
					EndIf
				Case index_player_select_start_game;START BUTTON
					;vsMode=1
					
					gameCommand = cmd_game_start
					;get player counts
					pl_c = 0
					For k=1 To g_playerCounts
						If ButSeq(k)<>player_type_none then pl_c = pl_c + 1
					Next
					
					;get team counts
					team_counts = 0
					last_team = -1
					For k=1 To g_playerCounts
						;print "k="+k+" zOn(k)="+zOn(k)+" zteam(k)="+zteam(k)
						If zOn(k)=1
							If zteam(k)=team_none and k>1
								team_counts = 100
								exit
							else
								If last_team <> zteam(k)then
									;print "k="+k+"zteam(k)="+zteam(k)
									team_counts = team_counts + 1
									last_team = zteam(k)
								EndIf
							EndIf
						EndIf
					Next
					;print "team_counts="+team_counts
					
					;check player counts,more than 1
					If pl_c < 1 Then
						warning=1
						warnSeq=0
						gameCommand=cmd_none
						strWarning$=strInfo$(index_str_players_needed)
					Else
						If vsMode=1 then
							If team_counts<2 then
								gameCommand=command_none
								warning=1
								warnSeq=1
								strWarning$=strInfo$(index_str_at_least_two_teams)
							Else
								If vsGameMode=mode_vs_catch_flag Then
									For k=1 To g_playerCounts
										If zteam(k)<>team_red and zOn(k)=1 and zteam(k)<>team_green Then;checks so that everyone has a team on CTF mode
											gameCommand=command_none
											warning=1:warnSeq=0:strWarning$=strInfo$(index_str_all_players_must_have_a_team)
											Exit
										EndIf
									Next
								EndIf
							EndIf
						Else;adventure mode
						EndIf
					EndIf
					If gameCommand = cmd_none Then
						If gamesound Then
							PlaySound brokensnd
						EndIf
					EndIf
					
				;select character
				Case index_player_select_character1
					If characterOpen(1)=1 Then curGuy(m_curSelected)=1:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+1
					If characterOpen(2)=1 Then curGuy(m_curSelected)=2:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+2
					If characterOpen(3)=1 Then curGuy(m_curSelected)=3:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+3
					If characterOpen(4)=1 Then curGuy(m_curSelected)=4:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+4
					If characterOpen(5)=1 Then curGuy(m_curSelected)=5:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+5
					If characterOpen(6)=1 Then curGuy(m_curSelected)=6:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+6
					If characterOpen(7)=1 Then curGuy(m_curSelected)=7:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+7
					If characterOpen(8)=1 Then curGuy(m_curSelected)=8:zThumbNail(m_curSelected)=butpic(n)
			    Case index_player_select_character1+8
					If characterOpen(9)=1 Then curGuy(m_curSelected)=9:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+9
					If characterOpen(10)=1 Then curGuy(m_curSelected)=10:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+10
					If characterOpen(11)=1 Then curGuy(m_curSelected)=11:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+11
					If characterOpen(12)=1 Then curGuy(m_curSelected)=12:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+12
					If characterOpen(13)=1 Then curGuy(m_curSelected)=13:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+13
					If characterOpen(14)=1 Then curGuy(m_curSelected)=14:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+14
					If characterOpen(15)=1 Then curGuy(m_curSelected)=15:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+15
					If characterOpen(16)=1 Then curGuy(m_curSelected)=16:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+16
					If characterOpen(17)=1 Then curGuy(m_curSelected)=17:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+17
					If characterOpen(18)=1 Then curGuy(m_curSelected)=18:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+18
					If characterOpen(19)=1 Then curGuy(m_curSelected)=19:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+19
					If characterOpen(20)=1 Then curGuy(m_curSelected)=20:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+20
					If characterOpen(21)=1 Then curGuy(m_curSelected)=21:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+21
					If characterOpen(22)=1 Then curGuy(m_curSelected)=22:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+22
					If characterOpen(23)=1 Then curGuy(m_curSelected)=23:zThumbNail(m_curSelected)=butpic(n)
				Case index_player_select_character1+23
					If characterOpen(24)=1 Then curGuy(m_curSelected)=24:zThumbNail(m_curSelected)=butpic(n)

				;select player type
				Case index_player_select_player_type1
					k = 1
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter1
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+1
					k = 2
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter2
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+2
					k = 3
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter3
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+3
					k = 4
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter4
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+4
					k = 5
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter5
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+5
					k = 6
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter6
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+6
					k = 7
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter7
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+7
					k = 8
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter8
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+8
					k = 9
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter9
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select
				Case index_player_select_player_type1+9
					k = 10
					Select ButSeq(k)
						Case player_type_none;"none"
							ButSeq(k)=player_type_cpu
							zOn(k)=1
							zAI(k)=1
						.anotherCharacter10
							curGuy(k)=Rand(1,characterAmount)
							zThumbNail(k)=butpic(curGuy(k))
				    		If characterOpen(curGuy(k))=0 Then Goto anotherCharacter1
							If vsMode=0 Then ButSeq(k)=player_type_human:zOn(k)=0:zAI(k)=0
						Case player_type_cpu;"cpu"
							ButSeq(k)=player_type_human
							zOn(k)=1
							zAI(k)=0
						Case player_type_human;"hum"
							ButSeq(k)=player_type_none
							zOn(k)=0
							zAI(k)=0
					End Select

				;team select
				Case index_player_select_player_team1
					k=1
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+1
					k=2
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+2
					k=3
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+3
					k=4
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+4
					k=5
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+5
					k=6
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+6
					k=7
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+7
					k=8
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+8
					k=9
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0
				Case index_player_select_player_team1+9
					k=10
					zTeam(k)=zTeam(k)+1
					If zTeam(k) > 2 Then zTeam(k)=0

				;click left arrow
				Case index_player_select_player_arrow1_l
			        selectPlayer(1,n,0)
			    Case index_player_select_player_arrow1_l+1
			    	selectPlayer(2,n,0)
			    Case index_player_select_player_arrow1_l+2
			    	selectPlayer(3,n,0)
			    Case index_player_select_player_arrow1_l+3
			    	selectPlayer(4,n,0)
			    Case index_player_select_player_arrow1_l+4
			    	selectPlayer(5,n,0)
			    Case index_player_select_player_arrow1_l+5
			    	selectPlayer(6,n,0)
			    Case index_player_select_player_arrow1_l+6
			    	selectPlayer(7,n,0)
			    Case index_player_select_player_arrow1_l+7
			    	selectPlayer(8,n,0)
			    Case index_player_select_player_arrow1_l+8
			    	selectPlayer(9,n,0)
			    Case index_player_select_player_arrow1_l+9
			    	selectPlayer(10,n,0)

				;click right arrow
				Case index_player_select_player_arrow1_r
					selectPlayer(1,n,1)
				Case index_player_select_player_arrow1_r+1
			    	selectPlayer(2,n,1)
			    Case index_player_select_player_arrow1_r+2
			    	selectPlayer(3,n,1)
			    Case index_player_select_player_arrow1_r+3
			    	selectPlayer(4,n,1)
			    Case index_player_select_player_arrow1_r+4
			    	selectPlayer(5,n,1)
			    Case index_player_select_player_arrow1_r+5
			    	selectPlayer(6,n,1)
			    Case index_player_select_player_arrow1_r+6
			    	selectPlayer(7,n,1)
			    Case index_player_select_player_arrow1_r+7
			    	selectPlayer(8,n,1)
			    Case index_player_select_player_arrow1_r+8
			    	selectPlayer(9,n,1)
			    Case index_player_select_player_arrow1_r+9
			    	selectPlayer(10,n,1)


				;level select
				Case index_player_select_level1
					k=1
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+1
					k=2
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+2
					k=3
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+3
					k=4
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+4
					k=5
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+5
					k=6
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+6
					k=7
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+7
					k=8
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+8
					k=9
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1
				Case index_player_select_level1+9
					k=10
					aiLevel(k)=aiLevel(k)+1
					If aiLevel(k) > 5 Then aiLevel(k)=1

				;rect selector
				Case index_player_select_selector1
					m_curSelected = 1
				Case index_player_select_selector1+1
					m_curSelected = 2
				Case index_player_select_selector1+2
					m_curSelected = 3
				Case index_player_select_selector1+3
					m_curSelected = 4
				Case index_player_select_selector1+4
					m_curSelected = 5
				Case index_player_select_selector1+5
					m_curSelected = 6
				Case index_player_select_selector1+6
					m_curSelected = 7
				Case index_player_select_selector1+7
					m_curSelected = 8
				Case index_player_select_selector1+8
					m_curSelected = 9
				Case index_player_select_selector1+9
					m_curSelected = 10

			End Select
			If gamesound And butOn(n)=1 Then bPlaySound=true
		EndIf
	Next

	If bPlaySound then PlaySound clicksnd
	
	zamountPlaying=0
	For n=1 To g_playerCounts
		If zOn(n)=1 Then zamountPlaying=zamountplaying+1
	Next

	If g_bDebug then print "rending character select scene..."
	
	;---------- redner menu (char screen) -----------------
	;----------Buttons attributes--------------------------
	x=0
	y=0
	buttonAmount = 100
	fontType = 2

	;-----Rendering menu --------------------------------
	TileImage backg,xtileimg,ytileimg
	;xtileimg=xtileimg+.1:ytileimg=ytileimg+.1

	;draw game mode,lives,scores
	offsetX = 10
	offsetY = 10
	spaceX = 10
	spaceY = 5
	sStr$ = ""

	If g_bDebug then print "drawing team attack..."
	
	;draw team attack
	If vsMode=1 Then
		If teamAttack=1 Then
			sStr$ = strInfo$(2)
		EndIf
		If teamAttack=0 Then
			sStr$ = strInfo$(3)
		EndIf
		drawTextImage_n(offsetX,offsetY,img_but_ta_bkgnd,sStr$,index_player_select_team_attack)
		offsetX = offsetX + ImageWidth(img_but_ta_bkgnd) + spaceX
	EndIf

	If g_bDebug then print "drawing game mode..."
	;draw "Game Mode"
	If vsMode=1 Then
		drawTextImage_n(offsetX,offsetY,img_but_gm_bkgnd,strInfo$(17) + gmStr$,index_player_select_game_mode)
		offsetX = offsetX + ImageWidth(img_but_gm_bkgnd) + spaceX
	EndIf

	;draw game mode options
	If g_bDebug then print "drawing game mode options..."

	If vsMode=1 Then
		layer_offset_x = 0
		layer_offset_y = offsetY
		layer_pad_left = 5
		layer_pad_right = 5
		layer_offset_y = offsetY + (ImageHeight(img_but_ta_bkgnd) - ImageHeight(arrow1))/2
		DrawImage img_but_ta_bkgnd,offsetX,offsetY

		;draw arrows
		DrawImage_n arrow1,offsetX+layer_pad_left,layer_offset_y,index_player_select_lives_left
		DrawImage_n arrow2,offsetX+ImageWidth(img_but_ta_bkgnd)-layer_pad_right-ImageWidth(arrow2),layer_offset_y,index_player_select_lives_right

		sTmp$ = ""
		If vsGameMode=mode_vs_death_match Then
			sTmp$ = strInfo$(index_str_lives) + gameLives
		EndIf
		If vsGameMode=mode_vs_catch_flag Then
			sTmp$ = strInfo$(index_str_score) + flagMAxScore
		EndIf
		If vsGameMode=mode_vs_keep_flag Then
			sTmp$ = strInfo$(index_str_time) + flagMAxTime
		EndIf
		If vsGameMode=mode_vs_hit_target Then
			sTmp$ = strInfo$(index_str_score) + targetMAxScore
		EndIf
		layer_offset_x = offsetX  + (ImageWidth(img_but_ta_bkgnd) - strWidth(sTmp$))/2
		layer_offset_y = offsetY + (ImageHeight(img_but_ta_bkgnd) - strHeight(""))/2
		
		pri layer_offset_x,layer_offset_y,sTmp$
		offsetX = offsetX + ImageWidth(img_but_ta_bkgnd) + spaceX
	EndIf

	;items on/off
	If g_bDebug then print "drawing items switch..."
	If noItems=1 Then
		sStr$ = strInfo$(97)
	Else
	    sStr$ = strInfo$(96)
	EndIf
	drawTextImage_n(offsetX,offsetY,img_but_sound_bkgnd,sStr$,index_player_select_items_switch)

	offsetY = offsetY + ImageHeight(img_but_sound_bkgnd) + spaceY*2
	
	;position of character pad
	ccount_line = 12;place 12 characters per line
	total_lines = 2;place 2 lines
	spaceX=0;space between 2 pads

	board3_width=ImageWidth(board3)
	board3_height=ImageHeight(board3)

	;caculate the offset x
	offsetX = getDrawOffsetXn(board3_width,spacex,ccount_line)

	If g_bDebug then print "drawing characters for select..."
	;draw characters to select
	For b = index_player_select_character1 To index_player_select_character1+characterAmount-1
		bb = b - index_player_select_character1
		x = bb mod ccount_line * (board3_width + spacex) + offsetX
		y = bb / ccount_line * (board3_height + spacey) + offsetY
		
		;draw background pad of character
		drawimage board3,x,y
		
		;Color 100,100,100:Rect xbut(b),ybut(b),wbut(b),hBut(b),1
		;Color 200,200,200:Rect xbut(b),ybut(b),wbut(b),hBut(b),0
		bb = bb + 1
		img = butpic2(bb)
		If characterOpen(bb) <> 1 Then
			img = lock
		EndIf
		x1 = (board3_width - ImageWidth(img))/2
		y1 = (board3_height - ImageHeight(img))/2 ;+ zbase(bb)
		DrawImage_n img,x+x1,y+y1,b
	Next

	If g_bDebug then print "drawing players for select..."
	;draw board, player # and pad
	ccount_line = 5;place 5 characters per line
	total_lines = 2;place 2 lines
	spaceX=10;space between 2 pads
	spaceY=5;space between 2 lines
	board_width=ImageWidth(board)
	board_height=ImageHeight(board)
	pad_width=ImageWidth(pad)
	pad_height=ImageHeight(pad)
	;caculate the offset x
	offsetX = getDrawOffsetXn(board_width,spaceX,ccount_line)
	offsetY = offsetY + total_lines * (board3_height + spaceY)
	n=0
	space2=49;vertical space between board and pad
	space_pad_x = 15
	space_pad_y = 12

	For i=1 To g_playerCounts
		n=n+1
		
		;draw board, player # and pad
		x = (i-1) mod ccount_line * (board_width + spaceX) + offsetX
		y = (i-1) / ccount_line * (board_height + space2 + pad_height + spaceY) + offsetY

		fillPosition(x-2,y-2,board_width+4,board_height+space2+pad_height+4,index_player_select_selector1+i-1)

		drawimage board,x,y;draw board
		pri x+space_pad_x,y+space_pad_y,strInfo$(12)+ n;draw "player:"
		pad_x = x + (board_width-pad_width)/2
		pad_y = y + board_height + space2
		drawimage pad,pad_x,pad_y;draw span

		;draw player type
		x_player_type = x + board_width - ImageWidth(butHum) - space_pad_x
		y_player_type = y + 10

		;draw player type
		;Color 200,200,200
		;Rect xbut(b),ybut(b),wbut(b),hBut(b),0
		b = index_player_select_player_type1+i-1
		Select butSeq(n)
			Case player_type_none:DrawImage_n butNA,x_player_type,y_player_type,b
			Case player_type_human:DrawImage_n butHum,x_player_type,y_player_type,b
			Case player_type_cpu:DrawImage_n butCPU,x_player_type,y_player_type,b
		End Select

		;team, selected player
		border_width = 84
		border_height = 20
		border_x = x + (board_width - border_width)/2
		border_y = y + board_height - 10 - border_height
		border_text_x = border_x + 5
		text_width = strWidth(strInfo$(31))
		text_height = strHeight(strInfo$(31))
		rect_width = border_width - text_width - 10
		rect_height = text_height
		border_text_y = border_y + (border_height-text_height)/2
		If vsMode=1 Then
			Color 200,200,200
			Rect_n border_x,border_y,border_width,border_height,0,index_player_select_player_team1+i-1
			
			If zTeam(n)<>team_red and zTeam(n)<>team_green Then
				Color 200,200,200:pri border_text_x,border_text_y,strInfo$(30);"TEAM:NONE"
			Else
				pri border_text_x,border_text_y,strInfo$(31);"TEAM:"
				border_rect_x = border_text_x + strWidth(strInfo$(31)) + 1
				border_rect_y = border_text_y
				Select zteam(n)
				Case team_red:Color 250,0,0:Rect border_rect_x,border_rect_y,rect_width,rect_height,1
				Case team_green:Color 0,250,0:Rect border_rect_x,border_rect_y,rect_width,rect_height,1
				End Select
			EndIf
		EndIf

		If butSeq(n) <> player_type_none
			;draw guys
			If CurGuy(n) > 0 Then
				img = butpic2(CurGuy(n))
				w = ImageWidth(img)
				h = ImageHeight(img)
				tempx = pad_x + (pad_width - w) / 2
				tempy = pad_y + pad_height/2 - h + zbase(CurGuy(n))
				DrawImage img,tempx,tempy
			EndIf

			;draws arrows For selecting player
			n1 = index_player_select_player_arrow1_l + i -1
			n2 = index_player_select_player_arrow1_r + i -1
			player_select_arrow_x = pad_x + 2
			player_select_arrow_x2 = pad_x + pad_width - ImageWidth(arrow2) - 2
			player_select_arrow_y = pad_y - 5 - ImageHeight(arrow1)
			;If n>6 then print "x1:"+player_select_arrow_x+" x2:"+player_select_arrow_x2+" y"+player_select_arrow_y+" img:"+arrow2
			DrawImage_n arrow1,player_select_arrow_x,player_select_arrow_y,n1
			DrawImage_n arrow2,player_select_arrow_x2,player_select_arrow_y,n2

			;Level Select For AI
			If butSeq(n)=player_type_cpu Then
				ai_level_x = x + (board_width - ImageWidth(board2))/2
				ai_level_y = pad_y + pad_height - ImageHeight(board2) + 5
				drawTextImage_n ai_level_x,ai_level_y,board2,strInfo$(32) + aiLevel(n),index_player_select_level1+i-1
			EndIf
		EndIf

		If i=m_curSelected
			;draw selected border
			
			;Color 0,255,0
			;Rect x-2,y-2,board_width+4,board_height+space2+pad_height+4,0
			If m_curSelectedSwitch>5
				m_curSelectedSwitch=0
				drawImage board_selector,x-2,y-2,0
			Else
				m_curSelectedSwitch=m_curSelectedSwitch+1
				drawImage board_selector,x-2,y-2,1
			EndIf
		EndIf
	Next

	;Draw Start the game!
	y = g_height - ImageHeight(pointerPic(1)) - ImageHeight(img_but_start_bkgnd) - 25
	drawCenterTextImage_n(y,img_but_start_bkgnd,strInfo$(1),index_player_select_start_game)
	
	For n=1 To 4
		DrawImage pointerPic(n),xpointer(n),ypointer(n)
		;pri xpointer(n),ypointer(n)-5," "+xpointer(n)+"."+ypointer(n)
	Next

	If warning=1 Then
		fontType=2
		warningMsg()
		fontType=1
	EndIf

End Function 

;----------- Warning on menu -------------------------------------------------
Function warningMsg()
	warnSeq=warnSeq+1
	If warnSeq > 140 Then warning=0
	strw = strWidth(strWarning$)
	strH = strHeight(strWarning$)
	w=strWidth(strWarning$)+20: h=strHeight(strWarning$)+4
	If h<30 then h=30
	x = (g_width - w) / 2
	y = (g_height - h) / 2
	Color 192,192,192
	Rect x,y,w,h,1
	Color Rand(1,255),Rand(1,255),Rand(1,255)
	x = x + 2
	y = y + 2
	w = w - 4
	h = h - 4
	Rect x,y,w,h,0
	pri x+(w-strW)/2,y+(h-strH)/2, strWarning$

End Function 

;----------- Main menu ---------------------------------------------------------
Function menuMain()
	
	pointers
	
	If KeyHit(14) Then  ;BACKSPACE key
		cheatSeq(1)=cheatSeq(1)+1
	EndIf

	If KeyHit(28) Then  ;ENTER key
		If cheatSeq(1)=5 And cheat(1)=0 Then
			cheat(1)=1  ;cheat_1 activated
			If gameSound Then PlaySound energySnd
	        For n=1 To characterAmount ;unlock all playable characters
	            characterOpen(n)=1
		    Next
		EndIf
		For n=1 To maxVsMap
			vsMapOpen(n)=1
		Next
		cheatSeq(1)=0
	EndIf

	For n=1 To ButtonAmount
		If clickedBut(n) Then
			Select n
				Case index_main_adventure_mode: menuOption=menu_select_character:vsMode=0 :defineButtons(1)
				Case index_main_vs_mode:menuOption=menu_select_character:vsMode=1      ;go to character Select screen (vs mode)
				Case index_main_options: menuOption=menu_options 			   ;go to options screen
				Case index_main_credits: rollCredits()
				Case index_main_exit: saveConfig() : end
			End Select
			If gamesound Then PlaySound ddhitsnd
		EndIf
	Next

	butText$(1)=strInfo$(index_str_adventure_mode);"adventure mode"
	butText$(2)=strInfo$(index_str_vs_mode);"vs mode"
	butText$(3)=strInfo$(index_str_options);"OPTIONS"
	butText$(4)=strInfo$(index_str_credits);"CREDITS"
	butText$(5)=strInfo$(index_str_exit);"EXIT"

	;-----Rendering menu --------------------------------
	fontType=1
	offsetY = 240
	For b=1 To 5
		pri_n priW(butText$(b)), offsetY, butText$(b),b
		offsetY = offsetY + strHeight(butText$(b)) + 32
	Next

	For n=1 To 4
		DrawImage pointerPic(n),xpointer(n),ypointer(n)
	Next

End Function

;----------- in game menu ---------------------------------------------------------
Function menuInGame()

	pointers
	
	buttonAmount = 4
	
	For n=1 To ButtonAmount
		If clickedBut(n) Then
			Select n
				Case index_in_game_resume: menuOption=menu_none: gamePaused = 0    ;resume game
				Case index_in_game_options: menuOption=menu_options;go to options screen
				Case index_in_game_main_menu: menuOption=menu_main: gameDone=1:gamePaused=0	;quit to main menu
				Case index_in_game_exit_game: saveConfig() : end              ;quit game
			End Select
			If gamesound Then PlaySound ddhitsnd
		EndIf
	Next

	;----------Buttons attributes--------------------------
	x=0 : y=0

	butText$(1)=strInfo$(index_str_resume)
	butText$(2)=strInfo$(index_str_options)
	butText$(3)=strInfo$(index_str_main_menu)
	butText$(4)=strInfo$(index_str_exit_game)

	;-----Rendering menu --------------------------------
	fontType=1
	offsetY = 240
	For b=1 To 5
		pri_n priW(butText$(b)), offsetY, butText$(b),b
		offsetY = offsetY + strHeight(butText$(b)) + 32
	Next
	
	For n=1 To 4
		DrawImage pointerPic(n),xpointer(n),ypointer(n)
	Next

End Function 
;----------- Options menu ---------------------------------------------------------
Function menuOptions()
	pointers
	
	For n=1 To index_options_maximaze
		If clickedBut(n) Then
			Select n
				Case index_options_full_screen
					If windowMode=0
						windowMode=1
					Else
						windowMode=0
				       	warning=1
				       	warnSeq=0
				       	strWarning$=strInfo$(index_str_must_restart_to_take_effect)
				       	saveConfig()
				    EndIf
				Case index_options_video
					If videoColorDepth=16
						videoColorDepth=32
					Else
						videoColorDepth=16
				       	warning=1
				       	warnSeq=0
				       	strWarning$=strInfo$(index_str_must_restart_to_take_effect)
				       	saveConfig()
					EndIf
				Case index_options_sfx
					If gameSound=1 Then gameSound=0 Else gameSound=1
				Case index_options_music
					If gameMusic=1 Then
						gameMusic=0
						stopchannel chMusic
					Else
					    gameMusic=1
					    LoopSound music
						chMusic = PlaySound(music)
				   	EndIf
				Case index_options_controls
					menuOption=menu_control
				Case index_options_language
					If curIdiom=lang_english Then curIdiom=lang_chinese Else curIdiom=lang_english
					idioms(curIdiom)
				Case index_options_back:
					If duringGameMenu=1 Then menuOption=5 Else menuOption=2
			End Select
			If gamesound Then PlaySound ddhitsnd
		EndIf
	Next

	;----------Buttons attributes--------------------------
	x=0 : y=0
	buttonAmount = 7
	For b= 1 To buttonAmount
		If b=7 Then y=y+50
		xBut(b)=180+x:yBut(b)=200+y:wBut(b)=290:hBut(b)=22
		y=y+30
	Next

	butText$(1)=strInfo$(19)
	butText$(2)=strInfo$(18)
	butText$(3)=strInfo$(45)
	butText$(4)=strInfo$(46)
	butText$(5)=strInfo$(49)
	butText$(6)=strInfo$(16)
	butText$(7)=strInfo$(48)

	;-----Rendering menu --------------------------------
	fontType=1
	For b=1 To buttonAmount
		pri xBut(b), yBut(b), butText$(b)
		;color 200,200,200
		;rect xBut(b),yBut(b),wBut(b),hBut(b),0
	Next
	o=200
	If windowMode=0 Then
	    pri xBut(1)+o, yBut(1), strInfo$(41)
	Else
	    pri xBut(1)+o, yBut(1), strInfo$(42)
	EndIf

	If videoColorDepth=16 Then
	    pri xBut(2)+o, yBut(2), strInfo$(43)
	Else
	    pri xBut(2)+o, yBut(2), strInfo$(44)
	EndIf

	If gameSound=1 Then
	    pri xBut(3)+o, yBut(3), strInfo$(41)
	Else
	    pri xBut(3)+o, yBut(3), strInfo$(42)
	EndIf

	If gameMusic=1 Then
	    pri xBut(4)+o, yBut(4), strInfo$(41)
	Else
	    pri xBut(4)+o, yBut(4), strInfo$(42)
	EndIf

	pri xBut(6)+o, yBut(6), strInfo$(47)

	For n=1 To 4
		DrawImage pointerPic(n),xpointer(n),ypointer(n)
	Next

End Function 

;----------- Controls menu ---------------------------------------------------------
Function menuControls()

pointers

For n=1 To ButtonAmount
If clickedBut(n) Then
  If gamesound Then PlaySound ddhitsnd
  Select n
	Case 1: setcontroller(1)
	Case 2: setcontroller(2)
	Case 3: setcontroller(3)
	Case 4: setcontroller(4)
	Case 5: configKeys(1)
	Case 6: configKeys(2)
	Case 7: configKeys(3)
	Case 8: configKeys(4)
	Case 9: menuOption = 3 :savekeys()
  End Select
	
EndIf
Next

;----------Buttons attributes--------------------------
x=100: y=220
buttonAmount = 13
For b= 1 To 4
	xBut(b) = x : yBut(b) = y : wBut(b)=100:hBut(b)=60

	If zController(b)=0 Then
		tpic(b) = keyboardPic
	Else
	    tPic(b) = controllerPic
	EndIf

	x=x+120
Next

x=100: y=290
For b= 5 To 8
	xBut(b) = x : yBut(b) = y : wBut(b)=100:hBut(b)=20
	x=x+120
	butText$(b) = strInfo$(60)
Next

x=135 : y=192
For b=10 To 13
    xBut(b) = x : yBut(b) = y : wBut(b)=0:hBut(b)=0
	x=x+120
	butText$(b) = "p"+(b-9)
Next

xBut(9)=100 : yBut(9)=420: wBut(9)=80: hBut(9)=25: butText$(9)=strInfo$(48)

;-----Rendering menu --------------------------------
fontType=1
For b=1 To 8
	If b < 5 Then
		drawimage tPic(b), xBut(b), yBut(b)
		If zController(b)=1 Then pri xBut(b)+2,yBut(b)+2,controllerPort(b)+1
   EndIf
	If b > 4 Then pri xBut(b), yBut(b), butText$(b)
Next

For b=10 To 13
	pri xBut(b),yBut(b),butText$(b)
Next

pri xBut(9), yBut(9), butText$(9)

;draw 4 pointers
For n=1 To 4
	DrawImage pointerPic(n),xpointer(n),ypointer(n)
Next

End Function 

;----------Idioms----------------------
Function idioms(n)
	curIdiom=n
	Select n
		Case lang_english
			strInfo$(index_str_start_game)					= "START GAME"
			strInfo$(index_str_team_attack_on)				= "Team Attack = ON"
			strInfo$(index_str_team_attack_off)				= "Team Attack = OFF"
			strInfo$(index_str_sound_on)					= "SOUND = ON"
			strInfo$(index_str_sound_off)					= "SOUND = OFF"
			strInfo$(index_str_death_match)					= "DEATH MATCH"
			strInfo$(index_str_capture_the_flag)			= "CAPTURE THE FLAG"
			strInfo$(index_str_keep_the_flag)				= "KEEP THE FLAG"
			strInfo$(index_str_hit_the_target)				= "HIT THE TARGET"
			strInfo$(index_str_time)						= "TIME: "
			strInfo$(index_str_score)						= "SCORE: "
			strInfo$(index_str_player)						= "PLAYER "
			strInfo$(index_str_won)							= " WON!!!"
			strInfo$(index_str_adventure_mode)				= "ADVENTURE MODE"
			strInfo$(index_str_options)						= "OPTIONS"
			strInfo$(index_str_language)					= "LANGUAGE"
			strInfo$(index_str_game_mode)					= "GAME MODE:"
			strInfo$(index_str_video)						= "VIDEO"
			strInfo$(index_str_full_screen)					= "FULL SCREEN"
			strInfo$(index_str_window_mode)					= "WINDOW MODE"
			strInfo$(index_str_vs_mode)						= "VS MODE"
			strInfo$(index_str_stage_select)				= "S T A G E   S E L E C T"
			strInfo$(index_str_loading)						= "L O A D I N G . . ."
			strInfo$(index_str_red_team_won)				= "RED TEAM WON!!!"
			strInfo$(index_str_green_team_won)				= "GREEN TEAM WON!!!"
			strInfo$(index_str_red)							= "RED"
			strInfo$(index_str_green)						= "GREEN"
			strInfo$(index_str_team_won)					= " TEAM WON!!!"
			strInfo$(index_str_lives)						= "LIVES: "
			strInfo$(index_str_team_none)					= "TEAM: NONE"
			strInfo$(index_str_team)						= "TEAM: "
			strInfo$(index_str_level)						= "LEVEL: "
			strInfo$(index_str_all_players_must_have_a_team)= "All players MUST have a TEAM."
			strInfo$(index_str_draw_game)					= "DRAW GAME!"
			strInfo$(index_str_secret_areas)				= "SECRET AREAS:"
			strInfo$(index_str_stage_complete)				= "STAGE COMPLETE!"
			strInfo$(index_str_press_any_key)				= "PRESS ANY KEY"
			strInfo$(index_str_hits_taken)					= "HITS TAKEN: "
			strInfo$(index_str_untouchable)					= "UNTOUCHABLE!"
			strInfo$(index_str_credits)						= "CREDITS"
			strInfo$(index_str_on)							= "ON"
			strInfo$(index_str_off)							= "OFF"
			strInfo$(index_str_16_bits)						= "16 BITS"
			strInfo$(index_str_32_bits)						= "32 BITS"
			strInfo$(index_str_sfx)							= "SFX"
			strInfo$(index_str_music)						= "MUSIC"
			strInfo$(index_str_english)						= "ENGLISH"
			strInfo$(index_str_back)						= "BACK"
			strInfo$(index_str_controls)					= "CONTROLS"
			strInfo$(index_str_exit)						= "EXIT"
			strInfo$(index_str_up)							= "UP"
			strInfo$(index_str_down)						= "DOWN"
			strInfo$(index_str_left)						= "LEFT"
			strInfo$(index_str_right)						= "RIGHT"
			strInfo$(index_str_attack)						= "ATTACK"
			strInfo$(index_str_special)						= "SPECIAL"
			strInfo$(index_str_jump)						= "JUMP"
			strInfo$(index_str_block)						= "BLOCK"
			strInfo$(index_str_throw)						= "THROW"
			strInfo$(index_str_config)						= "CONFIG."
			strInfo$(index_str_new_character_unlocked)		= "NEW CHARACTER UNLOCKED!"
			strInfo$(index_str_new_vs_stage_unlocked)		= "NEW VS STAGE UNLOCKED!"
			strInfo$(index_str_resume)						= "RESUME"
			strInfo$(index_str_main_menu)					= "MAIN MENU"
			strInfo$(index_str_exit_game)					= "EXIT GAME"
			strInfo$(index_str_must_restart_to_take_effect)	= "MUST RESTART TO TAKE EFFECT"
			strInfo$(index_str_press_jump_while_in)			= "press jump while in"
			strInfo$(index_str_the_air_to_double_jump)		= "the air to double jump."
			strInfo$(index_str_press_up_special)			= "press up + special"
			strInfo$(index_str_while_in_the_air_to)			= "while in the air to"
			strInfo$(index_str_go_up_even_further)			= "go up even further."
			strInfo$(index_str_press_attack_or_special)		= "press attack or special"
			strInfo$(index_str_to_fight_you_can_use)		= "to fight, you can use"
			strInfo$(index_str_combinations_by_pressing)	= "combinations by pressing"
			strInfo$(index_str_up_or_down_ex_down_special)	= "up or down. ex: down + special."
			strInfo$(index_str_stand_close_to_the_switch)	= "stand close to the switch"
			strInfo$(index_str_and_press_up_to_use_it)		= "and press up to use it."
			strInfo$(index_str_stand_on_an_item_and)		= "stand on an item and"
			strInfo$(index_str_press_attack_to_pick_it_up)	= "press attack to pick it up."
			strInfo$(index_str_then_press_throw_or_attack)	= "then press throw or attack"
			strInfo$(index_str_to_throw_it)					= "to throw it."
			strInfo$(index_str_to_go_down_from_platforms)	= "to go down from platforms"
			strInfo$(index_str_press_down_jump_when)		= "press down + jump when"
			strInfo$(index_str_standing_on_it)				= "standing on it."
			strInfo$(index_str_you_can_throw_items_at)		= "you can throw items at"
			strInfo$(index_str_different_directions_even)	= "different directions, even"
			strInfo$(index_str_diagonally_simply_hold)		= "diagonally, simply hold"
			strInfo$(index_str_the_direction_you_wish_to)	= "the direction you wish to"
			strInfo$(index_str_throw_and_press_attack)		= "throw and press attack"
			strInfo$(index_str_when_the_red_bar_at_the_top)	= "when the red bar at the top"
			strInfo$(index_str_of_the_screen_turns_green)	= "of the screen turns green,"
			strInfo$(index_str_you_can_release_a_super_move)= "you can release a super move"
			strInfo$(index_str_by_pressing_block_special)	= "by pressing block + special."
			strInfo$(index_str_no_air_special)				= "no air special."
			strInfo$(index_str_secrets_found)				= "secrets found: "
			strInfo$(index_str_items_on)					= "items: on"
			strInfo$(index_str_items_off)					= "items: off"
			strInfo$(index_str_continue)					= "CONTINUE"
			strInfo$(index_str_at_least_two_teams)			= "NEED AT LEAST 2 TEAMS TO START!"
			strInfo$(index_str_players_needed)	= "Choose at least 1 player to start!"
		Case lang_chinese
			strInfo$(index_str_start_game)					= "START GAME"
			strInfo$(index_str_team_attack_on)				= "Team Attack = ON"
			strInfo$(index_str_team_attack_off)				= "Team Attack = OFF"
			strInfo$(index_str_sound_on)					= "SOUND = ON"
			strInfo$(index_str_sound_off)					= "SOUND = OFF"
			strInfo$(index_str_death_match)					= "DEATH MATCH"
			strInfo$(index_str_capture_the_flag)			= "CAPTURE THE FLAG"
			strInfo$(index_str_keep_the_flag)				= "KEEP THE FLAG"
			strInfo$(index_str_hit_the_target)				= "HIT THE TARGET"
			strInfo$(index_str_time)						= "TIME: "
			strInfo$(index_str_score)						= "SCORE: "
			strInfo$(index_str_player)						= "PLAYER "
			strInfo$(index_str_won)							= " WON!!!"
			strInfo$(index_str_adventure_mode)				= "ADVENTURE MODE"
			strInfo$(index_str_options)						= "OPTIONS"
			strInfo$(index_str_language)					= "LANGUAGE"
			strInfo$(index_str_game_mode)					= "GAME MODE:"
			strInfo$(index_str_video)						= "VIDEO"
			strInfo$(index_str_full_screen)					= "FULL SCREEN"
			strInfo$(index_str_window_mode)					= "WINDOW MODE"
			strInfo$(index_str_vs_mode)						= "VS MODE"
			strInfo$(index_str_stage_select)				= "S T A G E   S E L E C T"
			strInfo$(index_str_loading)						= "L O A D I N G . . ."
			strInfo$(index_str_red_team_won)				= "RED TEAM WON!!!"
			strInfo$(index_str_green_team_won)				= "GREEN TEAM WON!!!"
			strInfo$(index_str_red)							= "RED"
			strInfo$(index_str_green)						= "GREEN"
			strInfo$(index_str_team_won)					= " TEAM WON!!!"
			strInfo$(index_str_lives)						= "LIVES: "
			strInfo$(index_str_team_none)					= "TEAM: NONE"
			strInfo$(index_str_team)						= "TEAM: "
			strInfo$(index_str_level)						= "LEVEL: "
			strInfo$(index_str_all_players_must_have_a_team)= "All players MUST have a TEAM."
			strInfo$(index_str_draw_game)					= "DRAW GAME!"
			strInfo$(index_str_secret_areas)				= "SECRET AREAS:"
			strInfo$(index_str_stage_complete)				= "STAGE COMPLETE!"
			strInfo$(index_str_press_any_key)				= "PRESS ANY KEY"
			strInfo$(index_str_hits_taken)					= "HITS TAKEN: "
			strInfo$(index_str_untouchable)					= "UNTOUCHABLE!"
			strInfo$(index_str_credits)						= "CREDITS"
			strInfo$(index_str_on)							= "ON"
			strInfo$(index_str_off)							= "OFF"
			strInfo$(index_str_16_bits)						= "16 BITS"
			strInfo$(index_str_32_bits)						= "32 BITS"
			strInfo$(index_str_sfx)							= "SFX"
			strInfo$(index_str_music)						= "MUSIC"
			strInfo$(index_str_english)						= "ENGLISH"
			strInfo$(index_str_back)						= "BACK"
			strInfo$(index_str_controls)					= "CONTROLS"
			strInfo$(index_str_exit)						= "EXIT"
			strInfo$(index_str_up)							= "UP"
			strInfo$(index_str_down)						= "DOWN"
			strInfo$(index_str_left)						= "LEFT"
			strInfo$(index_str_right)						= "RIGHT"
			strInfo$(index_str_attack)						= "ATTACK"
			strInfo$(index_str_special)						= "SPECIAL"
			strInfo$(index_str_jump)						= "JUMP"
			strInfo$(index_str_block)						= "BLOCK"
			strInfo$(index_str_throw)						= "THROW"
			strInfo$(index_str_config)						= "CONFIG."
			strInfo$(index_str_new_character_unlocked)		= "NEW CHARACTER UNLOCKED!"
			strInfo$(index_str_new_vs_stage_unlocked)		= "NEW VS STAGE UNLOCKED!"
			strInfo$(index_str_resume)						= "RESUME"
			strInfo$(index_str_main_menu)					= "MAIN MENU"
			strInfo$(index_str_exit_game)					= "EXIT GAME"
			strInfo$(index_str_must_restart_to_take_effect)	= "MUST RESTART TO TAKE EFFECT"
			strInfo$(index_str_continue)					= "CONTINUE"
			strInfo$(index_str_at_least_two_teams)			= "NEED AT LEAST 2 TEAMS TO START!"
			strInfo$(index_str_players_needed)	= "Choose at least 1 player to start!"
			;strInfo$(index_str_start_game)					= "???????��"
			;strInfo$(index_str_team_attack_on)				= "???��???? = ?��??"
			;strInfo$(index_str_team_attack_off)				= "???��???? = ??��?"
			;strInfo$(index_str_sound_on)					= "?��?? = ?��??"
			;strInfo$(index_str_sound_off)					= "?��?? = ??��?"
			;strInfo$(index_str_death_match)					= "???????��"
			;strInfo$(index_str_capture_the_flag)			= "?��?��"
			;strInfo$(index_str_keep_the_flag)				= "��????����??��??"
			;strInfo$(index_str_hit_the_target)				= "???��???��"
			;strInfo$(index_str_time)						= "?��??: "
			;strInfo$(index_str_score)						= "��???: "
			;strInfo$(index_str_player)						= "???? "
			;strInfo$(index_str_won)							= "?��??!!!"
			;strInfo$(index_str_adventure_mode)				= "?��??????"
			;strInfo$(index_str_options)						= "????"
			;strInfo$(index_str_language)					= "????"
			;strInfo$(index_str_game_mode)					= "???��????:"
			;strInfo$(index_str_video)						= "????"
			;strInfo$(index_str_full_screen)					= "????"
			;strInfo$(index_str_window_mode)					= "?��??"
			;strInfo$(index_str_vs_mode)						= "????"
			;strInfo$(index_str_stage_select)				= "????????"
			;strInfo$(index_str_loading)						= "?????? . . ."
			;strInfo$(index_str_red_team_won)				= "?��???��??!!!"
			;strInfo$(index_str_green_team_won)				= "?????��??!!!"
			;strInfo$(index_str_red)							= "?��??"
			;strInfo$(index_str_green)						= "????"
			;strInfo$(index_str_team_won)					= " ???��?��??!!!"
			;strInfo$(index_str_lives)						= "?��?��: "
			;strInfo$(index_str_team_none)					= "???��: ??"
			;strInfo$(index_str_team)						= "???��: "
			;strInfo$(index_str_level)						= "????: "
			;strInfo$(index_str_all_players_must_have_a_team)= "?��??????��??????????��."
			;strInfo$(index_str_draw_game)					= "????!"
			;strInfo$(index_str_secret_areas)				= "???????��:"
			;strInfo$(index_str_stage_complete)				= "???��?��??!"
			;strInfo$(index_str_press_any_key)				= "��??????��"
			;strInfo$(index_str_hits_taken)					= "��????��????: "
			;strInfo$(index_str_untouchable)					= "????????!"
			;strInfo$(index_str_credits)						= "?��??"
			;strInfo$(index_str_on)							= "?��??"
			;strInfo$(index_str_off)							= "??��?"
			;strInfo$(index_str_16_bits)						= "16 BITS"
			;strInfo$(index_str_32_bits)						= "32 BITS"
			;strInfo$(index_str_sfx)							= "???��"
			;strInfo$(index_str_music)						= "????"
			;strInfo$(index_str_english)						= "????"
			;strInfo$(index_str_back)						= "��???"
			;strInfo$(index_str_controls)					= "????"
			;strInfo$(index_str_exit)						= "????"
			;strInfo$(index_str_up)							= "??"
			;strInfo$(index_str_down)						= "??"
			;strInfo$(index_str_left)						= "����"
			;strInfo$(index_str_right)						= "??"
			;strInfo$(index_str_attack)						= "???��"
			;strInfo$(index_str_special)						= "????"
			;strInfo$(index_str_jump)						= "????"
			;strInfo$(index_str_block)						= "��???"
			;strInfo$(index_str_throw)						= "????"
			;strInfo$(index_str_config)						= "????."
			;strInfo$(index_str_new_character_unlocked)		= "??????????!"
			;strInfo$(index_str_new_vs_stage_unlocked)		= "??????????????!"
			;strInfo$(index_str_resume)						= "????"
			;strInfo$(index_str_main_menu)					= "?��????"
			;strInfo$(index_str_exit_game)					= "???????��"
			;strInfo$(index_str_must_restart_to_take_effect)	= "???????��?��?��?��"
			strInfo$(index_str_press_jump_while_in)			= "press jump while in"
			strInfo$(index_str_the_air_to_double_jump)		= "the air to double jump."
			strInfo$(index_str_press_up_special)			= "press up + special"
			strInfo$(index_str_while_in_the_air_to)			= "while in the air to"
			strInfo$(index_str_go_up_even_further)			= "go up even further."
			strInfo$(index_str_press_attack_or_special)		= "press attack or special"
			strInfo$(index_str_to_fight_you_can_use)		= "to fight, you can use"
			strInfo$(index_str_combinations_by_pressing)	= "combinations by pressing"
			strInfo$(index_str_up_or_down_ex_down_special)	= "up or down. ex: down + special."
			strInfo$(index_str_stand_close_to_the_switch)	= "stand close to the switch"
			strInfo$(index_str_and_press_up_to_use_it)		= "and press up to use it."
			strInfo$(index_str_stand_on_an_item_and)		= "stand on an item and"
			strInfo$(index_str_press_attack_to_pick_it_up)	= "press attack to pick it up."
			strInfo$(index_str_then_press_throw_or_attack)	= "then press throw or attack"
			strInfo$(index_str_to_throw_it)					= "to throw it."
			strInfo$(index_str_to_go_down_from_platforms)	= "to go down from platforms"
			strInfo$(index_str_press_down_jump_when)		= "press down + jump when"
			strInfo$(index_str_standing_on_it)				= "standing on it."
			strInfo$(index_str_you_can_throw_items_at)		= "you can throw items at"
			strInfo$(index_str_different_directions_even)	= "different directions, even"
			strInfo$(index_str_diagonally_simply_hold)		= "diagonally, simply hold"
			strInfo$(index_str_the_direction_you_wish_to)	= "the direction you wish to"
			strInfo$(index_str_throw_and_press_attack)		= "throw and press attack"
			strInfo$(index_str_when_the_red_bar_at_the_top)	= "when the red bar at the top"
			strInfo$(index_str_of_the_screen_turns_green)	= "of the screen turns green,"
			strInfo$(index_str_you_can_release_a_super_move)= "you can release a super move"
			strInfo$(index_str_by_pressing_block_special)	= "by pressing block + special."
			strInfo$(index_str_no_air_special)				= "no air special."
			strInfo$(index_str_secrets_found)				= "secrets found: "
			strInfo$(index_str_items_on)					= "items: on"
			strInfo$(index_str_items_off)					= "items: off"

	End Select

	For i=1 To 100
		strInfo$(i)=Lower(strInfo$(i))
	Next

End Function

;--------------------------------- Controls mouse pointers ------------------------
Function pointers()
	;zero button clicked flags
	For n=1 To buttonAmount
		clickedBut(n)=0:clickedBy(n)=0
	Next

	;zero players flags
	For n=1 To 30
		upKey(n)=0:leftKey(n)=0:rightKey(n)=0:downKey(n)=0:jumpKey(n)=0:shotKey(n)=0
		jumpKeyDown(n)=0:runkey(n)=0:blockKey(n)=0:specialkey(n)=0
	Next

	;place mouse point here
	xpointer(1)=MouseX(canvas)
	ypointer(1)=MouseY(canvas)

	For n= 2 To g_playerCounts
	Select zController(n)
	Case 0
		If KeyDown(upK(n)) Then upKey(n)=1
		If KeyDown(downK(n)) Then downKey(n)=1
		If KeyDown(leftK(n)) Then leftKey(n)=1
		If KeyDown(rightK(n)) Then rightKey(n)=1
		If KeyHit(shotK(n)) Then shotKey(n)=1

	Case 1
		If JoyYDir(controllerPort(n))=-1 Then upKey(n)=1
		If JoyXDir(controllerPort(n))=-1 Then leftKey(n)=1
		If JoyXDir(controllerPort(n))=1 Then rightKey(n)=1
		If JoyYDir(controllerPort(n))=1 Then downkey(n)=1
		If JoyHit(shotK(n),controllerPort(n)) Then shotKey(n)=1

	End Select
	Next

	pve=5
	For n=1 To 4
		If upKey(n)=1 Then
			ypointer(n)=ypointer(n)-pve
		EndIf
		If downKey(n)=1 Then
			ypointer(n)=ypointer(n)+pve
		EndIf
		If leftKey(n)=1 Then
			xpointer(n)=xpointer(n)-pve
		EndIf
		If rightKey(n)=1 Then
			xpointer(n)=xpointer(n)+pve
		EndIf
		If shotKey(n)=1 Then
			clickButton(n)
		EndIf
		
		If xpointer(n) < 1 Then
			xpointer(n) = 1
		EndIf
		If xpointer(n) > g_checkWidth Then
			xpointer(n) = g_checkWidth
		EndIf
		If ypointer(n) < 1 Then
			ypointer(n) = 1
		EndIf
		If ypointer(n) > g_checkHeight Then
			ypointer(n) = g_checkHeight
		EndIf
	Next

	If MouseHit(1) Then clickbutton(1)
End Function 

;------------ define buttons For vs/adventure mode -----------------
Function defineButtons(n)
	If n = 0 Then   ;For vs mode
		For i=1 To 100
			buton(i)=1
		Next
		
	EndIf

	If n = 1 Then;For adventure mode
		For i=1 To 4
			If zAi(i)=1 Then zAi(i)=0:zOn(i)=0:ButSeq(59+i)=0
		Next
		For i= 50 To 58
			butOn(i)=0
		Next
		For i= 64 To 67
			butOn(i)=0
		Next
		For i= 71 To 79
			butOn(i)=0
		Next
	EndIf

End Function

;------------ closing screens ----------------------------------------------------
Function closeScreen(n,s)
	If curWindowMode=0 Then
		If n > 0 Then SetBuffer FrontBuffer()
	Else
		SetBuffer CanvasBuffer(canvas)
	EndIf

	Color 10,10,10

	xw = g_width + 1
	xh = g_height + 1
	nStep = 10
	Select n
		Case method_horizontal_line	;horizontal lines
			y1=0 : y2=g_height
			For i=1 To 240
				Rect 0,y1,xw,nStep,1
				Rect 0,y2,xw,nStep,1
				If curWindowMode=1 Then
					FlipCanvas canvas
				Else
					VWait
				EndIf
				y1=y1+nStep
				y2=y2-nStep
				If y1 > g_height/2 Then Exit
			Next
		Case method_vertical_line	;vertical lines
			x1=0 : x2=xw
			For i=1 To 320
				Rect x1,0,nStep,xh,1
				Rect x2,0,nStep,xh,1
				If curWindowMode=1 Then
					FlipCanvas canvas
				Else
					VWait
				EndIf
				x1=x1+nStep
				x2=x2-nStep
				If x1 > g_width/2 Then Exit
			Next

		Case method_horizontal_vertical	;horizontal and vertical lines
			x1=0 : x2=xw
			y1=0 : y2=g_height
			For i=1 To 320
				Rect x1,0,nStep,xh,1
				Rect x2,0,nStep,xh,1
				Rect 0,y1,xw,nStep,1
				Rect 0,y2,xw,nStep,1
				If curWindowMode=1 Then
					FlipCanvas canvas
				Else
					VWait
				EndIf
				x1=x1+nStep : x2=x2-nStep
				y1=y1+nStep : y2=y2-nStep
				If x1 > g_width/2 Then Exit
			Next
		Case method_vertical_spread ;spreading vertical lines
			x1=32
			For i=1 To 68 Step 4
				For ii= 0 To g_width Step 80
					Rect x1+ii,0,i,xh,1
				Next
				x1=x1-2
				If curWindowMode=1 Then
					FlipCanvas canvas
				Else
					VWait
				EndIf
			Next
	End Select

End Function 


;---------- BUTTON CLICK -----------------------------------------------
Function clickButton(n)
	For nn=1 To buttonAmount
		;print "x:"+xpointer(n)+" y:"+ypointer(n)+" xbut:"+xBut(nn)+" ybut:"+yBut(nn)+" w:"+wbut(nn)+" h:"+hbut(nn)+" on:"+butOn(nn)
		If xpointer(n) => xBut(nn) And xpointer(n) =< xbut(nn)+wbut(nn) And butOn(nn) Then
			If ypointer(n) => yBut(nn) And ypointer(n) =< ybut(nn)+hbut(nn) Then
				clickedBut(nn)=1:clickedBy(nn)=n:
				;print "clicked,nn:"+nn+" n:"+n
			EndIf
		EndIf
	Next
End Function

;----------- Set controller type ----------------------------
Function setController(n)

	If zController(n)=0 Then
		zController(n)=1 : controllerPort(n)=0
	ElseIf controllerPort(n)=0 Then
		controllerPort(n)=1
	ElseIf controllerPort(n)=1 Then
		controllerPort(n)=2
	ElseIf controllerPort(n)=2 Then
		controllerPort(n)=3
	ElseIf controllerPort(n)=3 Then
		zController(n)=0 : controllerPort(n)=99
	EndIf

End Function

;-------------Configure Keys!-----------------------------------------------------------------
Function configKeys(player)

pn = player

If zController(pn) =1 Then  keyschosen=5 Else keyschosen=1

Repeat
setbuffer CanvasBuffer(canvas)
FlushKeys()

For b=1 To 8
	If b < 5 Then
	    drawimage tPic(b), xBut(b), yBut(b)
		If zController(b)=1 Then pri xBut(b)+2,yBut(b)+2,controllerPort(b)+1
	EndIf
	If b > 4 Then pri xBut(b), yBut(b), butText$(b)
Next

For b=10 To 13
	pri xBut(b),yBut(b),butText$(b)
Next

If pn=1 Then x = 100
If pn=2 Then x = 220
If pn=3 Then x = 340
If pn=4 Then x = 460
y = 320
color 28,28,28
rect x,y,220,20,1

Select keyschosen
Case 1:pri x,y,strInfo$(51): FlipCanvas canvas:upK(pn)=getTheKey(controllerPort(pn))
Case 2:pri x,y,strInfo$(52): FlipCanvas canvas:downK(pn)=getTheKey(controllerPort(pn))
Case 3:pri x,y,strInfo$(53): FlipCanvas canvas:leftK(pn)=getTheKey(controllerPort(pn))
Case 4:pri x,y,strInfo$(54): FlipCanvas canvas:rightK(pn)=getTheKey(controllerPort(pn))
Case 5:pri x,y,strInfo$(55): FlipCanvas canvas:shotK(pn)=getTheKey(controllerPort(pn))
Case 6:pri x,y,strInfo$(56): FlipCanvas canvas:specialK(pn)=getTheKey(controllerPort(pn))
Case 7:pri x,y,strInfo$(57): FlipCanvas canvas:jumpK(pn)=getTheKey(controllerPort(pn))
Case 8:pri x,y,strInfo$(58): FlipCanvas canvas:blockK(pn)=getTheKey(controllerPort(pn))
Case 9:pri x,y,strInfo$(59): FlipCanvas canvas:grabK(pn)=getTheKey(controllerPort(pn))

End Select

keyschosen=keyschosen+1
If gameSound Then PlaySound clickSnd
Until keyschosen > 9
For i=1 To 50
  clickedBut(i)=0
Next

End Function 
;-------------------- Get Key pressed ---------------------
Function getTheKey(port)

pressed=0
Repeat
	For n=2 To 221
		If KeyHit(n) Then Return n
	Next
	If zcontroller(pn)=1 Then
		For n=1 To 10
			If JoyHit(n,port) Then Return n
		Next
	EndIf
Until KeyHit(1) = True

End Function 

;-------Load keys---------------------
Function loadkeys()

If FileType("keys.cfg")=0 Then
	temp=WriteFile("keys.cfg")	;create dumb file
	For n=1 To 60
		WriteInt temp, 0
	Next
	CloseFile temp
EndIf

file=ReadFile("keys.cfg")
For n=1 To 4

zController(n) = ReadInt (file)
controllerPort(n) = ReadInt (file)
upK(n) = ReadInt (file)
leftK(n)= ReadInt (file)
rightK(n)= ReadInt (file)
downK(n)= ReadInt (file)
shotK(n)= ReadInt (file)
specialK(n)= ReadInt (file)
jumpK(n)= ReadInt (file)
blockK(n)= ReadInt (file)
grabK(n)= ReadInt (file)

Next

CloseFile file

End Function 


;------------------save keys to file----
Function savekeys()
file=WriteFile("keys.cfg")
For n=1 To 4
	WriteInt file, zController(n)
	WriteInt file, controllerPort(n)
	WriteInt file, upK(n)
	WriteInt file, leftK(n)
	WriteInt file, rightK(n)
	WriteInt file, downK(n)
	WriteInt file, shotK(n)
	WriteInt file, specialK(n)
	WriteInt file, jumpK(n)
	WriteInt file, blockK(n)
	WriteInt file, grabK(n)
Next
CloseFile file

End Function 
;-------Load game config ---------------------
Function loadConfig()

If FileType("game.cfg")=0 Then ;create file If it doesn`t exist
	temp=WriteFile("game.cfg")	
	For n=1 To 20
		WriteInt temp, 0
	Next
	CloseFile temp
	Return False
EndIf

file=ReadFile("game.cfg")

windowMode = ReadInt (file)
videoColorDepth = ReadInt (file)
gameSound = ReadInt (file)
gameMusic = ReadInt (file)
curIdiom = ReadInt (file)
curModId = ReadInt (file)

CloseFile file

Return True

End Function 

;------------------save game config to file----
Function saveConfig()
	file=WriteFile("game.cfg")

	WriteInt file, windowMode
	WriteInt file, videoColorDepth
	WriteInt file, gameSound
	WriteInt file, gameMusic
	WriteInt file, curIdiom
	WriteInt file, curModId		;Current MOD id

	CloseFile file
End Function 
;-------------------- wait this time or keyhit ------------------
Function waitThis(n)
	time1 = millisecs()

	Repeat

		Delay(10)
		If KeyHit(1) Then keyh=1
		If KeyHit(28) Then keyh=1
		If KeyHit(57) Then keyh=1

	Until (MilliSecs() => time1 + n) Or keyh=1

End Function 
;--------------------- game intro --------------------------
Function gameIntro()
	justIntroduced=1
End Function 
;---------------------- Roll Credits ------------------------
Function rollCredits()

End Function 

Function getDrawOffsetX(img)
	width = (g_width - ImageWidth(img)) / 2
	return width
End Function

Function getDrawOffsetY(img)

End Function

Function getDrawOffsetXn(width,space,count)
	offsetX = (g_width - count * width - space*(count-1)) / 2
	return offsetX 
End Function

Function drawCenterTextImage_n(top,img,text$,index)
	w = ImageWidth(img)
	x = getDrawOffsetXn(w,0,1)
	drawTextImage_n(x,top,img,text$,index)
End Function

Function drawTextImage_n(left,top,img,text$,index)
	If fillPosition(left, top, ImageWidth(img), ImageHeight(img), index)=1
		drawTextImage(left,top,img,text$)
	EndIf
End Function

Function drawCenterTextImage(top,img,text$)
	w = ImageWidth(img)
	x = getDrawOffsetXn(w,0,1)
	drawTextImage(x,top,img,text$)
End Function

Function drawTextImage(left,top,img,text$)
	DrawImage img,left,top
	w = ImageWidth(img)
	h = ImageHeight(img)
	x = Left + (w - strWidth(text$))/2
	y = top + (h - strHeight(text$))/2
	pri x,y,text$
End Function

Function drawImage_n(img,x,y,index)
	If fillPosition(x, y, ImageWidth(img), ImageHeight(img), index)=1
		drawImage img,x,y
	EndIf
End Function

Function rect_n(x, y, width, height, solid, index)
	fillPosition(x, y, width, height, index)
	Rect(x, y, width, height, solid)
End Function

Function fillPosition(x, y, width, height, index)
	If index>0 and index<buttonAmount+1
		xBut(index) = x
		yBut(index) = y
		wBut(index) = width
		hBut(index) = height
		Return 1
	EndIf
	Return 0
End Function

Function checkClose()
	Select WaitEvent()
		Case $802	;window size
			While WindowMinimized(window)
				WaitEvent()	;should handle window close here...
			Wend
		Case $803	;window close
			If Confirm( "Really Quit?" ) End
	End Select
End Function